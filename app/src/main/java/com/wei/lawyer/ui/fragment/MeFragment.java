package com.wei.lawyer.ui.fragment;

import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import com.wei.lawyer.App;
import com.wei.lawyer.R;
import com.wei.lawyer.model.BaseDataBean;
import com.wei.lawyer.model.MeMenuBean;
import com.wei.lawyer.model.UserBean;
import com.wei.lawyer.model.WebDataBean;
import com.wei.lawyer.net.presenter.UserPresenter;
import com.wei.lawyer.ui.activity.CollectionActivity;
import com.wei.lawyer.ui.activity.ConsultingActivity;
import com.wei.lawyer.ui.activity.LawActivity;
import com.wei.lawyer.ui.activity.LoginActivity;
import com.wei.lawyer.ui.activity.OrderActivity;
import com.wei.lawyer.ui.activity.SettingActivity;
import com.wei.lawyer.ui.activity.SuggestActivity;
import com.wei.lawyer.ui.activity.WebViewActivity;
import com.wei.lawyer.ui.adapter.BaseDelegateAdapter;
import com.wei.lawyer.ui.adapter.BaseViewHolder;
import com.wei.lawyer.utils.ImageUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;

/**
 * 作者：赵若位
 * 时间：2018/10/27 13:45
 * 邮箱：1070138445@qq.com
 * 功能：个人中心
 */
public class MeFragment extends BaseFragment<UserPresenter>
{
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private DelegateAdapter mAdapter;
    private BaseDelegateAdapter mUserAdapter, mMenuAdapter;


    @Override
    protected int getLayoutResource()
    {
        return R.layout.fragment_me;
    }

    @Override
    protected UserPresenter createPresenter()
    {
        return new UserPresenter(this, this);
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void initView()
    {
        super.initView();
        VirtualLayoutManager manager = new VirtualLayoutManager(mActivity);
        manager.setInitialPrefetchItemCount(2);
        mRecyclerView.setLayoutManager(manager);
        //关闭动效提升效率
        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        mRecyclerView.setItemViewCacheSize(2);

        mAdapter = new DelegateAdapter(manager, false);
        mRecyclerView.setAdapter(mAdapter);
        initAdapter();
    }

    @Override
    protected void initData()
    {
        super.initData();
        if (TextUtils.isEmpty(App.mToken))
        {
            mUserAdapter.setData(new UserBean());
        } else
        {
            mPresenter.getUserInfo();
        }
    }

    /*初始化适配器*/
    private void initAdapter()
    {
        //用户信息
        mUserAdapter = new BaseDelegateAdapter<UserBean>(mActivity, R.layout.layout_me_user)
        {
            @Override
            public void convert(BaseViewHolder holder, int position, final UserBean data)
            {
                ImageUtils utils = new ImageUtils(mActivity);
                utils.setUserImageResource(data.getAvatar(), (ImageView) holder.getView(R.id.img_user));
                holder.setText(data != null && (!TextUtils.isEmpty(data.getNickName()))
                        ? data.getNickName() : getString(R.string.tv_no_login), R.id.tv_user)
                        .setText(data.getCashDescript(), R.id.tv_charge)
                        .setText(data.getPointDescript(), R.id.tv_point)
                        .itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if (data.getID() == 0)
                        {
                            openActivity(LoginActivity.class, null);
                        }
                    }
                });
            }
        };
        mAdapter.addAdapter(mUserAdapter);
        mUserAdapter.setData(new UserBean());


        //个人中心菜单
        mMenuAdapter = new BaseDelegateAdapter<MeMenuBean>(mActivity, R.layout.layout_me_menu)
        {
            @Override
            public void convert(BaseViewHolder holder, final int position, MeMenuBean data)
            {
                holder.setImageResource(data.getImageRescource(), R.id.item_image)
                        .setText(data.getTitle(), R.id.item_title)
                        .setText(data.getDescript(), R.id.item_descript)
                        .setVisible(R.id.item_line, position == 0 || position == 4)
                        .itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        onMenuClickListener(position);
                    }
                });
            }
        };
        mAdapter.addAdapter(mMenuAdapter);
        mMenuAdapter.setData(MeMenuBean.getList());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMain(BaseDataBean data)
    {
        if (data.getCode() == 200 &&
                (!TextUtils.isEmpty(data.getMsg())) &&
                data.getMsg().equals(MeFragment.class.getSimpleName()))
        {
            initData();
        }
    }


    /*菜单监听*/
    private void onMenuClickListener(int position)
    {
        switch (position)
        {
            case 0://我的咨询
                openActivity(ConsultingActivity.class, null);
                break;
            case 1://我的订单
                openActivity(OrderActivity.class, null);
                break;
            case 2://我的收藏
                openActivity(CollectionActivity.class, null);
                break;
            case 3://法条搜索
                openActivity(LawActivity.class, null);
                break;
            case 4://律师认证
                if (mActivity != null)
                {
                    mActivity.showMsgDialog(getString(R.string.dialog_lawyer_verify), new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            alert("下载成功...");
                            mActivity.disMsgDialog();
                        }
                    });
                }
                break;
            case 5://邀请好友
                WebDataBean data = new WebDataBean();
                data.setTitle(getString(R.string.tv_invite_friend));
                data.setUrl("https://www.jianshu.com/p/c6541452d6e2");
                openActivity(WebViewActivity.class, data);
                break;
            case 6://投诉建议
                openActivity(SuggestActivity.class, null);
                break;
            case 7://设置
                openActivity(SettingActivity.class, null);
                break;
            default:
                break;
        }
    }


    @Override
    public void showData(Object o)
    {
        super.showData(o);
        if (o instanceof UserBean)
        {
            mUserAdapter.setData((UserBean) o);
        }
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
