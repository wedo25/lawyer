package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;
import com.wei.lawyer.ui.adapter.BasePageAdapter;
import com.wei.lawyer.ui.fragment.HistoryOrderFragment;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/3 11:27
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class HistoryOrderActivity extends BaseActivity
{
    @Bind(R.id.tv_order)
    TextView mTvOrder;
    @Bind(R.id.tv_deposit)
    TextView mTvDeposit;
    @Bind(R.id.tv_order_line)
    TextView mTvOrderLine;
    @Bind(R.id.tv_deposit_line)
    TextView mTvDepositLine;
    @Bind(R.id.viewPager)
    ViewPager mViewPager;

    private BasePageAdapter mAdapter;

    private int lastPosition = -1;


    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_history_order;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_history_order));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int i, float v, int i1)
            {

            }

            @Override
            public void onPageSelected(int position)
            {
                setFragment(position);
            }

            @Override
            public void onPageScrollStateChanged(int i)
            {

            }
        });
        mAdapter = new BasePageAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mAdapter.setData(HistoryOrderFragment.getFragment("历史订单"));
        mAdapter.setData(HistoryOrderFragment.getFragment("历史定金"));
        setFragment(0);
    }


    private void setFragment(int position)
    {
        if (lastPosition == position) return;
        mTvOrder.setTextColor(getResources().getColor(R.color.textUnSelected));
        mTvOrderLine.setVisibility(View.GONE);

        mTvDeposit.setTextColor(getResources().getColor(R.color.textUnSelected));
        mTvDepositLine.setVisibility(View.GONE);
        switch (position)
        {
            case 0:
                mTvOrder.setTextColor(getResources().getColor(R.color.textTitle));
                mTvOrderLine.setVisibility(View.VISIBLE);
                break;
            case 1:
                mTvDeposit.setTextColor(getResources().getColor(R.color.textTitle));
                mTvDepositLine.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        mViewPager.setCurrentItem(position);
        lastPosition = position;
    }


    @OnClick({R.id.tv_order, R.id.tv_deposit})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.tv_order:
                setFragment(0);
                break;
            case R.id.tv_deposit:
                setFragment(1);
                break;
        }
    }
}
