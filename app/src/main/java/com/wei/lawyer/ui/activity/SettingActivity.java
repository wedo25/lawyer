package com.wei.lawyer.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.wei.lawyer.App;
import com.wei.lawyer.R;
import com.wei.lawyer.model.TokenBean;
import com.wei.lawyer.model.UserBean;
import com.wei.lawyer.net.BasePresenter;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/1 12:23
 * 邮箱：1070138445@qq.com
 * 功能：个人中心设置
 */
public class SettingActivity extends BaseActivity
{
    @Bind(R.id.tv_version_text)
    TextView mTvVersionText;
    @Bind(R.id.tv_cache_text)
    TextView mTvCacheText;
    @Bind(R.id.tv_exit)
    TextView mTvExit;

    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_setting;
    }

    @Override
    protected void setStatusBarColor()
    {

    }

    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }


    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_setting));
        mTvExit.setVisibility(TextUtils.isEmpty(App.mToken) ? View.GONE : View.VISIBLE);
    }

    /*判断当前账户是否已登录*/
    private void startOtherActivity(Class<? extends BaseActivity> cls)
    {
        if (TextUtils.isEmpty(App.mToken))
        {
            Intent target = new Intent(mActivity, SettingActivity.class);
            openActivity(LoginActivity.class, target);
        } else
        {
            openActivity(cls, null);
        }
    }


    @OnClick({R.id.tv_modify, R.id.tv_account, R.id.tv_version, R.id.tv_cache, R.id.tv_exit})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.tv_modify://编辑个人资料:
                startOtherActivity(ModifyUserActivity.class);
                break;
            case R.id.tv_account://账号和密码
                startOtherActivity(AccountActivity.class);
                break;
            case R.id.tv_version://当前版本
                break;
            case R.id.tv_cache://清理缓存
                break;
            case R.id.tv_exit://退出当前账号
                showMsgDialog(getString(R.string.dialog_exit_account), new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        TokenBean.delete();
                        App.mToken = null;
                        UserBean.updateUserInfo();//更新个人中心用户信息
                        openActivity(LoginActivity.class, null);
                        doFinish(200);
                    }
                });
                break;
        }
    }
}
