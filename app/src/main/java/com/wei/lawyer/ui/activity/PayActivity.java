package com.wei.lawyer.ui.activity;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;

/**
 * 作者：赵若位
 * 时间：2018/12/13 14:21
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class PayActivity extends BaseActivity
{
    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_pay;
    }

    @Override
    protected void setStatusBarColor()
    {

    }

    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }
}
