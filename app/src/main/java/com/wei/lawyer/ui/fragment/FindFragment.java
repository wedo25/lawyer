package com.wei.lawyer.ui.fragment;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.view.View;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.wei.lawyer.R;
import com.wei.lawyer.model.LawyerBean;
import com.wei.lawyer.model.LawyerSearchBean;
import com.wei.lawyer.net.presenter.FindPresenter;
import com.wei.lawyer.ui.adapter.LawyerListAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/10/27 13:45
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class FindFragment extends RootFragment<FindPresenter>
{
    @Bind(R.id.layout_content)
    RecyclerView mRecyclerView;
    @Bind(R.id.refresh)
    SmartRefreshLayout mRefresh;

    private LawyerListAdapter mAdapter;
    /*律师条件查询对象*/
    private LawyerSearchBean mSearch = new LawyerSearchBean();


    @Override
    protected int getLayoutResource()
    {
        return R.layout.fragment_find;
    }

    @Override
    protected FindPresenter createPresenter()
    {
        return new FindPresenter(this, this);
    }

    @Override
    protected void initView()
    {
        super.initView();
        setRefreshListener();
        LinearLayoutManager manager = new LinearLayoutManager(mActivity);
        manager.setInitialPrefetchItemCount(5);
        mRecyclerView.setLayoutManager(manager);
        //关闭动效提升效率
        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        //Item高度固定，避免浪费资源
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(5);
        mAdapter = new LawyerListAdapter(mActivity);
        mAdapter.bindRecyclerView(mRecyclerView);
    }


    /*设置上拉和下拉刷新的监听*/
    private void setRefreshListener()
    {
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener()
        {
            @Override
            public void onRefresh(RefreshLayout refresh)
            {
                mSearch.setPage(1);
                initData();
            }

            @Override
            public void onLoadMore(RefreshLayout refresh)
            {
                mSearch.increase();
                initData();
                refresh.finishLoadMore(3000);
            }
        });
    }


    @Override
    protected void initData()
    {
        super.initData();
        mPresenter.getLawyerList(mSearch);
    }

    @OnClick({R.id.img_chat, R.id.tv_search, R.id.tv_city, R.id.tv_skill, R.id.tv_sort})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.img_chat:
                showError();
                break;
            case R.id.tv_search:
//                openActivity(lasear);
                break;
            case R.id.tv_city:
                break;
            case R.id.tv_skill:
                break;
            case R.id.tv_sort:
                break;
        }
    }



    @Override
    public void showData(Object o)
    {
        super.showData(o);
        if (o instanceof List)
        {
            List<LawyerBean> list = (List<LawyerBean>) o;
            //下拉刷新
            if (mSearch.getPage() == 1)
            {
                mAdapter.setData(list);
            } else
            {
                mAdapter.setData(list, mAdapter.getData().size());
            }
        }
    }
}
