package com.wei.lawyer.ui.fragment;

import androidx.annotation.IntDef;
import androidx.annotation.LayoutRes;
import android.view.View;
import android.view.ViewGroup;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 作者：赵若位
 * 时间：2018/11/22 16:39
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public abstract class RootFragment<T extends BasePresenter> extends BaseFragment<T>
{
    /*主内容*/
    private static final int CONTENT = 0x00;
    /*错误页面*/
    private static final int ERROR = 0x01;

    private ViewGroup mContent, mParent;
    private View mError;


    @State
    private int mCurrent = CONTENT;


    @Override
    protected void initView()
    {
        super.initView();
        mContent = mRootView.findViewById(R.id.layout_content);
        if (mContent == null)
        {
            throw new IllegalStateException("The subclass of RootActivity must contain a View named 'layout_content'!");
        }
        if (!(mContent.getParent() instanceof ViewGroup))
        {
            throw new IllegalStateException("layout_content's ParentView should be a ViewGroup.");
        }
        mParent = (ViewGroup) mContent.getParent();
    }

    @Override
    public void showContent()
    {
        super.showContent();
        if (mCurrent == CONTENT)
        {
            return;
        }
        hideCurrentView();
        mCurrent = CONTENT;
        mContent.setVisibility(View.VISIBLE);
    }


    @Override
    public void showError()
    {
        super.showError();
        if (mCurrent == ERROR)
        {
            return;
        }
        if (mError == null)
        {
            View.inflate(mActivity, R.layout.layout_error, mParent);
            mError = mParent.findViewById(R.id.layout_error);
        }
        hideCurrentView();
        mCurrent = ERROR;
        mError.setVisibility(View.VISIBLE);
    }

    /*设置自定义错误布局*/
    protected void setErrorView(@LayoutRes int layout)
    {
        View.inflate(mActivity, layout, mParent);
        mError = mParent.findViewById(R.id.layout_error);
    }

    /*隐藏不需要显示的布局*/
    private void hideCurrentView()
    {
        switch (mCurrent)
        {
            case CONTENT:
                mContent.setVisibility(View.GONE);
                break;
            case ERROR:
                mError.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }


    @IntDef({CONTENT, ERROR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface State
    {

    }
}
