package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import com.alibaba.android.vlayout.layout.GridLayoutHelper;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.wei.lawyer.App;
import com.wei.lawyer.R;
import com.wei.lawyer.model.FieldBean;
import com.wei.lawyer.model.UserBean;
import com.wei.lawyer.model.WebDataBean;
import com.wei.lawyer.net.presenter.FindPresenter;
import com.wei.lawyer.ui.adapter.BaseDelegateAdapter;
import com.wei.lawyer.ui.adapter.BaseViewHolder;
import com.wei.lawyer.utils.RxUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Consumer;

/**
 * 作者：赵若位
 * 时间：2018/11/10 10:16
 * 邮箱：1070138445@qq.com
 * 功能：快速咨询
 */
public class QuickConsultingActivity extends BaseActivity<FindPresenter>
{
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private DelegateAdapter mAdapter;

    private BaseDelegateAdapter mTitleAdapter, mTypeAdapter, mPayAdapter;


    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_quick_consulting;
    }

    @Override
    protected void setStatusBarColor()
    {

    }

    @Override
    protected FindPresenter createPresenter()
    {
        return new FindPresenter(this, this);
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_home_quick));

        VirtualLayoutManager manager = new VirtualLayoutManager(mActivity);
        manager.setInitialPrefetchItemCount(3);
        mRecyclerView.setLayoutManager(manager);
        //关闭动效提升效率
        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        mRecyclerView.setItemViewCacheSize(3);

        mAdapter = new DelegateAdapter(manager);
        mRecyclerView.setAdapter(mAdapter);
        initAdapter();
    }

    /*初始化各个类型的适配器*/
    private void initAdapter()
    {
        mTitleAdapter = new BaseDelegateAdapter<String>(this, R.layout.layout_quick_consulting_process)
        {
            @Override
            public void convert(BaseViewHolder holder, int position, String s)
            {
                holder.setText(getString(R.string.tv_do_have_what_question), R.id.tv_title);
            }
        };
        mAdapter.addAdapter(mTitleAdapter);
        mTitleAdapter.setData("");

        //快速咨询的方向
        mTypeAdapter = new BaseDelegateAdapter<FieldBean>(this, R.layout.item_quick_consulting_field)
        {
            @Override
            public void convert(BaseViewHolder holder, final int position, FieldBean data)
            {
                holder.setText(data.getName(), R.id.item_title)
                        .setTextColor(getResources().getColor(lastPosition == position ? R.color.white : R.color.textUnSelected), R.id.item_title)
                        .setBackgroundResource(lastPosition == position ? R.drawable.shape_quick_consulting_selected : R.drawable.shape_quick_consulting_unselected, R.id.item_title)
                        .itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if (lastPosition == position) return;
                        lastPosition = position;
                        notifyDataSetChanged();
                    }
                });
            }

            @Override
            public LayoutHelper onCreateLayoutHelper()
            {
                GridLayoutHelper helper = new GridLayoutHelper(4);
                helper.setBgColor(getResources().getColor(R.color.white));
                helper.setAutoExpand(false);
                helper.setPaddingLeft(getResources().getDimensionPixelOffset(R.dimen.dp_14));
                helper.setPaddingRight(getResources().getDimensionPixelOffset(R.dimen.dp_14));
                return helper;
            }

            @Override
            protected int setLastPosition()
            {
                return 0;
            }
        };
        mAdapter.addAdapter(mTypeAdapter);

        //支付方式
        mPayAdapter = new BaseDelegateAdapter<UserBean>(this, R.layout.layout_quick_consulting_pay_type)
        {
            @Override
            public void convert(final BaseViewHolder holder, int position, UserBean data)
            {
                final EditText descript = holder.getView(R.id.et_descript);
                final TextView count = holder.getView(R.id.tv_count);
                descript.addTextChangedListener(new TextWatcher()
                {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after)
                    {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count)
                    {

                    }

                    @Override
                    public void afterTextChanged(Editable s)
                    {
                        StringBuilder builder = new StringBuilder();
                        builder.append(descript.getText().toString().length())
                                .append("/100");
                        count.setText(builder);
                    }
                });
                setPointUi((TextView) holder.getView(R.id.tv_pay_point), data.getPoint());
                setAccountUi((TextView) holder.getView(R.id.tv_pay_account), data.getCash());
                setUrgentUi((TextView) holder.getView(R.id.tv_pay_urgent_descript_1));

                holder.setText(getResources().getString(R.string.tv_recharge_point), R.id.tv_pay_point_price)
                        .setText(getResources().getString(R.string.tv_recharge_cash), R.id.tv_pay_account_price)
                        .setImageResource(R.mipmap.radio_bg_2, R.id.img_pay_point)
                        .setImageResource(R.mipmap.radio_bg_1, R.id.img_pay_account)
                        .setImageResource(R.mipmap.radio_bg_2, R.id.img_pay_urgent)
                        .setVisible(R.id.tv_pay_point_price, true)
                        .setVisible(R.id.tv_pay_account_price, false)
                        .setOnClickListener(R.id.tv_pay_point, new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                setPayType(holder, 0);
                            }
                        }).setOnClickListener(R.id.tv_pay_account, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        setPayType(holder, 1);
                    }
                });
            }

            //选择支付方式时候选中状态
            private void setPayType(BaseViewHolder holder, int status)
            {
                holder.setVisible(R.id.tv_pay_point_price, status == 0);
                holder.setImageResource(status == 0 ? R.mipmap.radio_bg_2 : R.mipmap.radio_bg_1, R.id.img_pay_point);

                holder.setVisible(R.id.tv_pay_account_price, status == 1);
                holder.setImageResource(status == 1 ? R.mipmap.radio_bg_2 : R.mipmap.radio_bg_1, R.id.img_pay_account);
            }

            //异步设置律币余额的UI样式
            private void setPointUi(final TextView view, final long point)
            {
                if (view != null)
                {
                    Observable.create(new ObservableOnSubscribe<SpannableString>()
                    {
                        @Override
                        public void subscribe(ObservableEmitter<SpannableString> e) throws Exception
                        {
                            String descript = getString(R.string.tv_point_balance);
                            SpannableString span = new SpannableString(descript + "  " + point);
                            ForegroundColorSpan text = new ForegroundColorSpan(getResources().getColor(R.color.textTitle));
                            span.setSpan(text, 0, descript.length(), SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
                            ForegroundColorSpan toast = new ForegroundColorSpan(getResources().getColor(R.color.toast));
                            span.setSpan(toast, descript.length(), span.length(), SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
                            e.onNext(span);
                            e.onComplete();
                        }
                    }).compose(RxUtils.<SpannableString>rxObservableSchedlers())
                            .compose(mActivity.<SpannableString>bindUntilEvent(ActivityEvent.DESTROY))
                            .subscribe(new Consumer<SpannableString>()
                            {
                                @Override
                                public void accept(SpannableString span) throws Exception
                                {
                                    view.setText(span);
                                }
                            });
                }
            }


            //异步设置账户余额的UI样式
            private void setAccountUi(final TextView view, final Double price)
            {
                if (view != null)
                {
                    Observable.create(new ObservableOnSubscribe<SpannableString>()
                    {
                        @Override
                        public void subscribe(ObservableEmitter<SpannableString> e) throws Exception
                        {
                            String descript = getString(R.string.tv_account_balance);
                            SpannableString span = new SpannableString(descript + "  ￥" + price / 100);
                            ForegroundColorSpan text = new ForegroundColorSpan(getResources().getColor(R.color.textTitle));
                            span.setSpan(text, 0, descript.length(), SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
                            ForegroundColorSpan toast = new ForegroundColorSpan(getResources().getColor(R.color.price));
                            span.setSpan(toast, descript.length(), span.length(), SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
                            e.onNext(span);
                            e.onComplete();
                        }
                    }).compose(RxUtils.<SpannableString>rxObservableSchedlers())
                            .compose(mActivity.<SpannableString>bindUntilEvent(ActivityEvent.DESTROY))
                            .subscribe(new Consumer<SpannableString>()
                            {
                                @Override
                                public void accept(SpannableString span) throws Exception
                                {
                                    view.setText(span);
                                }
                            });
                }
            }

            //异步设置平台加急UI样式
            private void setUrgentUi(final TextView view)
            {
                if (view != null)
                {
                    Observable.create(new ObservableOnSubscribe<SpannableString>()
                    {
                        @Override
                        public void subscribe(ObservableEmitter<SpannableString> e) throws Exception
                        {
                            String price = String.valueOf(App.mConfig != null ? App.mConfig.getUrgentChatPrice() / 100 : 0);
                            String descript = getString(R.string.tv_urgent_descript_1).replace("Price", price);
                            SpannableString span = new SpannableString(descript);
                            ForegroundColorSpan title = new ForegroundColorSpan(getResources().getColor(R.color.textTitle));
                            span.setSpan(title, 0, descript.length(), SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
                            ForegroundColorSpan color = new ForegroundColorSpan(getResources().getColor(R.color.price));
                            span.setSpan(color, 2, 3 + price.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                            e.onNext(span);
                            e.onComplete();
                        }
                    }).compose(RxUtils.<SpannableString>rxObservableSchedlers())
                            .compose(mActivity.<SpannableString>bindUntilEvent(ActivityEvent.DESTROY))
                            .subscribe(new Consumer<SpannableString>()
                            {
                                @Override
                                public void accept(SpannableString span) throws Exception
                                {
                                    view.setText(span);
                                }
                            });
                }
            }
        };
        mAdapter.addAdapter(mPayAdapter);
        mPayAdapter.setData(new UserBean());
    }


    @Override
    protected void initData()
    {
        super.initData();
        mPresenter.getLawyerSkill();
        mPresenter.getUserAssets();
    }

    @OnClick({R.id.img_wen, R.id.tv_kefu, R.id.tv_action})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.img_wen://咨询提示
                //TODO 咨询提示
                WebDataBean data = new WebDataBean();
                data.setTitle(getString(R.string.tv_consulting_prompt));
                data.setUrl("http://www.baidu.com");
                openActivity(WebViewActivity.class, data);
                break;
            case R.id.tv_kefu://联系客服
                openActivity(SuggestActivity.class, null);
                break;
            case R.id.tv_action://支付下单:
                alert(getString(R.string.alert_send_quick_order_success));
                doFinish(200);
                break;
        }
    }


    @Override
    public void showData(Object o)
    {
        super.showData(o);
        if (o instanceof List)
        {
            List<FieldBean> list = (List<FieldBean>) o;
            mTypeAdapter.setData(list);
        }

        if (o instanceof UserBean)
        {
            mPayAdapter.setData(o);
        }
    }
}
