package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/3 21:38
 * 邮箱：1070138445@qq.com
 * 功能：我的咨询
 */
public class ConsultingActivity extends BaseActivity
{
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Bind(R.id.refresh)
    SmartRefreshLayout mRefresh;

    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_consulting;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_consulting));
    }


    @OnClick(R.id.tv_history)
    public void onViewClicked()
    {
        openActivity(HistoryConsultingActivity.class, null);
    }
}
