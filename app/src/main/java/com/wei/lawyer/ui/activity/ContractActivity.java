package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.view.View;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.wei.lawyer.R;
import com.wei.lawyer.model.ContractBean;
import com.wei.lawyer.model.FieldBean;
import com.wei.lawyer.net.contract.ContractView;
import com.wei.lawyer.net.presenter.HomePresenter;
import com.wei.lawyer.ui.adapter.BaseAdapter;
import com.wei.lawyer.ui.adapter.BaseViewHolder;
import com.wei.lawyer.ui.adapter.ContractListAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/7 15:12
 * 邮箱：1070138445@qq.com
 * 功能：合同模板
 */
public class ContractActivity extends BaseActivity<HomePresenter> implements ContractView
{
    @Bind(R.id.refresh)
    SmartRefreshLayout mRefresh;
    @Bind(R.id.left)
    RecyclerView mLeft;
    @Bind(R.id.right)
    RecyclerView mRight;

    private BaseAdapter<FieldBean> mLabelAdapter;
    private BaseAdapter<ContractBean> mContractAdapter;

    //当前合同页数
    private int mPage = 1;


    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_contract;
    }

    @Override
    protected void setStatusBarColor()
    {

    }

    @Override
    protected HomePresenter createPresenter()
    {
        return new HomePresenter(this, this);
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_contract));
        setRefreshListener();
        initLabelAdapter();
        initContractAdapter();
    }

    /*页面刷新监听*/
    private void setRefreshListener()
    {
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener()
        {
            @Override
            public void onLoadMore(RefreshLayout refresh)
            {
                mPage += 1;
                initContractListData();
            }

            @Override
            public void onRefresh(RefreshLayout refresh)
            {
                mPage = 1;
                initContractListData();
            }
        });
    }

    /*初始化标签适配器*/
    private void initLabelAdapter()
    {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        mLeft.setLayoutManager(manager);
        //关闭动效提升效率
        ((SimpleItemAnimator) mLeft.getItemAnimator()).setSupportsChangeAnimations(false);
        //Item高度固定，避免浪费资源
        mLeft.setHasFixedSize(true);

        mLabelAdapter = new BaseAdapter<FieldBean>(this, R.layout.item_contract_label)
        {
            @Override
            public void convert(BaseViewHolder holder, final int position, final FieldBean data)
            {
                holder.setText(data.getName(), R.id.item_title)
                        .setBackgroundResource(lastPosition == position ? R.drawable.shape_contract_label : 0, R.id.item_title)
                        .setTextColor(lastPosition == position ? getResources().getColor(R.color.textTitle) : getResources().getColor(R.color.textUnSelected), R.id.item_title)
                        .itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if (lastPosition == position) return;
                        lastPosition = position;
                        /*获取合同模板列表*/
                        mPage = 1;
                        mPresenter.getContractList(data.getID(), mPage);
                        notifyDataSetChanged();
                    }
                });
            }

            @Override
            protected int setLastPosition()
            {
                return 0;
            }
        };
        mLabelAdapter.bindRecyclerView(mLeft);
    }

    /*初始化合同模板适配器*/
    private void initContractAdapter()
    {
        GridLayoutManager manager = new GridLayoutManager(this, 2);
        mRight.setLayoutManager(manager);
        //关闭动效提升效率
        ((SimpleItemAnimator) mRight.getItemAnimator()).setSupportsChangeAnimations(false);
        //Item高度固定，避免浪费资源
        mRight.setHasFixedSize(true);

//        mContractAdapter = new BaseAdapter<ContractBean>(this, R.layout.item_contract)
//        {
//            @Override
//            public void convert(BaseViewHolder holder, int position, final ContractBean data)
//            {
//                holder.setImageResource(data.getListView(), R.id.item_image)
//                        .setText(data.getTitle(), R.id.item_title)
//                        .setText(new StringBuilder().append(getString(R.string.tv_money))
//                                .append(data.getPrice() / 100d), R.id.item_price)
//                        .itemView.setOnClickListener(new View.OnClickListener()
//                {
//                    @Override
//                    public void onClick(View v)
//                    {
//                        openActivity(ContractDetailActivity.class, data);
//                    }
//                });
//            }
//        };
        mContractAdapter = new ContractListAdapter(mActivity);
        mContractAdapter.bindRecyclerView(mRight);
    }


    @Override
    protected void initData()
    {
        super.initData();
        mPresenter.getContractClassification();
    }

    /*加载合同模板列表数据*/
    private void initContractListData()
    {
        FieldBean data = mLabelAdapter.getSelectedData();
        if (data != null)
        {
            mPresenter.getContractList(data.getID(), mPage);
        }
    }

    @OnClick(R.id.img_search)
    public void onViewClicked()
    {
        openActivity(ContractSearchActivity.class, null);
    }

    @Override
    public void showClassification(List<FieldBean> list)
    {
        mLabelAdapter.setData(list);
        initContractListData();
    }

    @Override
    public void showContractData(List<ContractBean> list)
    {
        if (mPage == 1)
        {
            mContractAdapter.setData(list);
            mRefresh.setNoMoreData(false);
        } else
        {
            mContractAdapter.setData(list, mContractAdapter.getData().size());
            if (list.isEmpty())
            {
                mRefresh.setNoMoreData(true);
            }
        }
        stopRefreshingOrLoadMore();
    }
}
