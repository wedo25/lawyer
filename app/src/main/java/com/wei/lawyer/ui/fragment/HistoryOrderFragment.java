package com.wei.lawyer.ui.fragment;

import android.os.Bundle;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;

import butterknife.Bind;

/**
 * 作者：赵若位
 * 时间：2018/11/3 21:13
 * 邮箱：1070138445@qq.com
 * 功能：历史订单
 */
public class HistoryOrderFragment extends BaseFragment
{
    @Bind(R.id.tv)
    TextView mTv;


    public static HistoryOrderFragment getFragment(String s)
    {
        HistoryOrderFragment fragment = new HistoryOrderFragment();
        Bundle bundle = new Bundle();
        bundle.putString(HistoryOrderFragment.class.getSimpleName(),s);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutResource()
    {
        return R.layout.fragment_history_order;
    }

    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView()
    {
        super.initView();
        mTv.setText(getArguments().getString(this.getClass().getSimpleName()));
    }

}
