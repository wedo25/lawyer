package com.wei.lawyer.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 作者：赵若位
 * 时间：2018/11/3 10:29
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class OrderFragment extends BaseFragment
{
    @Bind(R.id.mTv)
    TextView mTv;

    private String mData;


    public static BaseFragment getFragment(String msg)
    {
        OrderFragment fragment = new OrderFragment();
        Bundle bundle = new Bundle();
        bundle.putString(OrderFragment.class.getSimpleName(), msg);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutResource()
    {
        return R.layout.fragment_order;
    }

    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView()
    {
        super.initView();
        mData = getArguments().getString(OrderFragment.class.getSimpleName());
        mTv.setText(mData);
    }


}
