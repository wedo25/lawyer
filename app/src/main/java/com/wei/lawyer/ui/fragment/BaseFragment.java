package com.wei.lawyer.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.trello.rxlifecycle2.components.support.RxFragment;
import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;
import com.wei.lawyer.net.BaseView;
import com.wei.lawyer.ui.activity.BaseActivity;
import com.wei.lawyer.utils.ProgressDialog;

import butterknife.ButterKnife;

/**
 * 作者：赵若位
 * 时间：2018/10/27 14:00
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public abstract class BaseFragment<T extends BasePresenter> extends RxFragment implements BaseView
{
    /*获取布局资源*/
    protected abstract int getLayoutResource();

    /*获取Presenter层对象*/
    protected abstract T createPresenter();


    protected T mPresenter;
    protected View mRootView;
    protected BaseActivity mActivity;
    private ProgressDialog mProgressDialog;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPresenter = createPresenter();
        mActivity = (BaseActivity) getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (mRootView == null)
        {
            mRootView = inflater.inflate(getLayoutResource(), container, false);
        }
        ButterKnife.bind(this, mRootView);
        initView();
        initData();
        return mRootView;
    }

    protected void initView()
    {

    }

    protected void initData()
    {
    }


    /*Toast弹窗*/
    protected void alert(String msg)
    {
        if (TextUtils.isEmpty(msg)) return;
        Toast.makeText(mActivity.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    /*启动Activity*/
    protected void openActivity(Class<? extends BaseActivity> cls, Parcelable parcelable)
    {
        Intent it = new Intent(mActivity, cls);
        if (parcelable != null)
        {
            it.putExtra(cls.getSimpleName(), parcelable);
        }
        startActivity(it);
        mActivity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }


    /*启动Activity*/
    protected void openActivityForResult(Class<? extends BaseActivity> cls, Parcelable parcelable, int requestCode)
    {
        Intent it = new Intent(mActivity, cls);
        if (parcelable != null)
        {
            it.putExtra(cls.getSimpleName(), parcelable);
        }
        startActivityForResult(it, requestCode);
        mActivity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    @Override
    public void showErrorMsg(String msg)
    {
        alert(msg);
    }

    @Override
    public void showError()
    {

    }

    @Override
    public void showLoading()
    {
        if (mProgressDialog == null)
        {
            mProgressDialog = new ProgressDialog(mActivity);
        }
        if (mProgressDialog.isShowing())
        {
            return;
        }
        mProgressDialog.show();
    }

    @Override
    public void dissLoading()
    {
        if (mProgressDialog != null && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showContent()
    {

    }

    @Override
    public void showData(Object o)
    {
        stopRefreshingOrLoadMore();
    }

    /*停止下拉刷新和上拉加载更多的动效*/
    private void stopRefreshingOrLoadMore()
    {
        if (mRootView == null) return;
        SmartRefreshLayout layout = mRootView.findViewById(R.id.refresh);
        if (layout != null)
        {
            if (layout.isRefreshing())
            {
                layout.finishRefresh(200);
            }

            if (layout.isLoading())
            {
                layout.finishLoadMore(200);
            }
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        ButterKnife.unbind(this);
        if (mPresenter != null)
        {
            mPresenter.detach();
        }
    }
}
