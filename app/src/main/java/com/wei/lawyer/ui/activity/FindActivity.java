package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;
import com.wei.lawyer.ui.fragment.FindFragment;

/**
 * 作者：赵若位
 * 时间：2018/10/31 18:35
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class FindActivity extends BaseActivity
{

    //TODO 添加返回按钮
    private FragmentManager mManager;

    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_find;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        mManager = getSupportFragmentManager();
        FragmentTransaction ft = mManager.beginTransaction();
        ft.add(R.id.layout, new FindFragment());
        ft.commit();
    }
}
