package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import android.widget.EditText;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/2 20:53
 * 邮箱：1070138445@qq.com
 * 功能：修改密码
 */
public class ModifyPasWordActivity extends BaseActivity
{
    @Bind(R.id.et_old)
    EditText mEtOld;
    @Bind(R.id.et_new)
    EditText mEtNew;
    @Bind(R.id.et_confirm)
    EditText mEtConfirm;

    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_modify_pasword;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_setting_modify_pass));

    }


    @OnClick(R.id.tv_action)
    public void onViewClicked()
    {
        //TODO 修改密码
    }
}
