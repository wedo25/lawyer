package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.model.WebDataBean;
import com.wei.lawyer.net.BasePresenter;

import butterknife.Bind;

/**
 * 作者：赵若位
 * 时间：2018/12/15 11:07
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class DescriptActivity extends BaseActivity
{
    @Bind(R.id.tv_descript)
    TextView mTvDescript;

    private WebDataBean mData;

    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_descript;
    }

    @Override
    protected void setStatusBarColor()
    {

    }

    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }


    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        mData = getIntent().getParcelableExtra(DescriptActivity.class.getSimpleName());
        setLeftBack();
        setTitle(mData != null ? mData.getTitle() : "");
        mTvDescript.setText(mData != null ? mData.getDescript() : "");
    }
}
