package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.model.TokenBean;
import com.wei.lawyer.net.presenter.UserPresenter;
import com.wei.lawyer.utils.LogUtils;
import com.wei.lawyer.utils.StringUtils;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/10/29 12:33
 * 邮箱：1070138445@qq.com
 * 功能：重置密码界面
 */
public class ForgetActivity extends BaseActivity<UserPresenter> implements TextWatcher
{
    @Bind(R.id.et_phone)
    EditText mEtPhone;
    @Bind(R.id.tv_code)
    TextView mTvCode;
    @Bind(R.id.et_code)
    EditText mEtCode;
    @Bind(R.id.et_passWord)
    EditText mEtPassWord;
    @Bind(R.id.tv_action)
    TextView mTvAction;

    /*当前是否正在进行验证码倒计时*/
    private boolean isCoding = false;

    /*短信倒计时器*/
    private Timer mTimer = new Timer();

    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_forget;
    }

    @Override
    protected void setStatusBarColor()
    {

    }

    @Override
    protected UserPresenter createPresenter()
    {
        return new UserPresenter(this, this);
    }


    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        mEtPhone.addTextChangedListener(this);
        mEtCode.addTextChangedListener(this);
        mEtPassWord.addTextChangedListener(this);
        mTvCode.setClickable(false);
        mTvAction.setClickable(false);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {

    }

    @Override
    public void afterTextChanged(Editable s)
    {
        setCodeBtn();
        setActionBtn();
    }

    /*设置获取验证码按钮UI样式*/
    private void setCodeBtn()
    {
        String account = mEtPhone.getText().toString();
        boolean click = (!TextUtils.isEmpty(account)) && account.length() == 11 && (!isCoding);
        mTvCode.setClickable(click);
        mTvCode.setBackgroundColor(getResources().getColor(click ? R.color.color : R.color.textBG));
        mTvCode.setTextColor(getResources().getColor(click ? R.color.black : R.color.textUnSelected));
    }

    /*设置充值密码按钮UI演示*/
    private void setActionBtn()
    {
        String account = mEtPhone.getText().toString();
        String code = mEtCode.getText().toString();
        String pass = mEtPassWord.getText().toString();
        boolean isClick = (!TextUtils.isEmpty(account)) && (!TextUtils.isEmpty(code)) && (!TextUtils.isEmpty(pass));
        mTvAction.setClickable(isClick);
        mTvAction.setBackgroundColor(getResources().getColor(isClick ? R.color.color : R.color.textBG));
        mTvAction.setTextColor(getResources().getColor(isClick ? R.color.black : R.color.textUnSelected));
    }


    @OnClick({R.id.tv_code, R.id.tv_action})
    public void onViewClicked(View view)
    {
        if (isFastDoubleClick()) return;
        String account = mEtPhone.getText().toString();
        if (TextUtils.isEmpty(account) || (!StringUtils.isMobile(account)))
        {
            alert(getString(R.string.alert_enter_phone));
            return;
        }
        switch (view.getId())
        {
            case R.id.tv_code://获取验证码:
                mPresenter.getPhoneCode(account, true);
                break;
            case R.id.tv_action://重置密码
                String code = mEtCode.getText().toString();
                if (TextUtils.isEmpty(code) || code.length() < 6)
                {
                    alert(getString(R.string.alert_enter_code));
                    return;
                }

                String pass = mEtPassWord.getText().toString();
                if (TextUtils.isEmpty(pass) || pass.length() < 6)
                {
                    alert(getString(R.string.alert_enter_passWord));
                    return;
                }
                mPresenter.forgetPassword(account, pass, code);
                break;
        }
    }

    @Override
    public void showData(Object o)
    {
        super.showData(o);
        if (o instanceof String)
        {
            alert((String) o);
            mTimer.start();
        }

        if (o instanceof TokenBean)
        {
            alert(getString(R.string.alert_login_success));
            ((TokenBean) o).insert();
            openActivity(MainActivity.class, null);
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (mTimer != null)
        {
            mTimer.cancel();
        }
    }

    /*短信验证码倒计时器*/
    public class Timer extends CountDownTimer
    {
        public Timer()
        {
            super(60 * 1000, 1000);
        }

        @Override
        public void onTick(long times)
        {
            StringBuilder builder = new StringBuilder();
            builder.append(times / 1000).append("s");
            mTvCode.setText(builder.toString());
            isCoding = true;
            setCodeBtn();
        }

        @Override
        public void onFinish()
        {
            isCoding = false;
            setCodeBtn();
            mTvCode.setText(getString(R.string.tv_getCode));
        }
    }
}

