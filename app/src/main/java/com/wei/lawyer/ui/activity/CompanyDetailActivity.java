package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import com.wei.lawyer.R;
import com.wei.lawyer.model.CompanyServiceBean;
import com.wei.lawyer.model.LawyerCommentBean;
import com.wei.lawyer.net.presenter.HomePresenter;
import com.wei.lawyer.ui.adapter.BaseAdapter;
import com.wei.lawyer.ui.adapter.BaseDelegateAdapter;
import com.wei.lawyer.ui.adapter.BaseViewHolder;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/14 17:58
 * 邮箱：1070138445@qq.com
 * 功能：服务详情
 */
public class CompanyDetailActivity extends BaseActivity<HomePresenter>
{
    @Bind(R.id.tv_action)
    TextView mTvAction;
    @Bind(R.id.layout_content)
    RecyclerView mRecyclerView;

    private DelegateAdapter mAdapter;

    private BaseDelegateAdapter mTitleAdapter, mServiceAdapter;

    private CompanyServiceBean mData;

    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_company_detail;
    }

    @Override
    protected void setStatusBarColor()
    {

    }

    @Override
    protected HomePresenter createPresenter()
    {
        return new HomePresenter(this, this);
    }


    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        mData = getIntent().getParcelableExtra(CompanyDetailActivity.class.getSimpleName());
        VirtualLayoutManager manager = new VirtualLayoutManager(mActivity);
        manager.setInitialPrefetchItemCount(3);
        mRecyclerView.setLayoutManager(manager);
        //关闭动效提升效率
        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        mRecyclerView.setItemViewCacheSize(3);

        mAdapter = new DelegateAdapter(manager);
        mRecyclerView.setAdapter(mAdapter);
        initAdapter();
    }


    /*初始化适配器*/
    private void initAdapter()
    {
        //标题
        mTitleAdapter = new BaseDelegateAdapter<CompanyServiceBean>(this, R.layout.layout_company_detail_introduce)
        {
            @Override
            public void convert(BaseViewHolder holder, final int position, final CompanyServiceBean data)
            {
                holder.setText(data.getService(), R.id.tv_title)
                        .setText(data.getTitle(), R.id.tv_descript)
                        .setText(new StringBuilder(getString(R.string.tv_selled)).append(data.getSalesVolume()), R.id.tv_sell)
                        .setRoundImageResource(data.getScreenshot(), getResources().getDimensionPixelOffset(R.dimen.dp_6), R.id.img_descript)
                        .setTextColor(getResources().getColor(lastPosition == 0 ? R.color.textTitle : R.color.textUnSelected), R.id.tv_detail)
                        .setTextColor(getResources().getColor(lastPosition == 1 ? R.color.textTitle : R.color.textUnSelected), R.id.tv_comments)
                        .setVisible(R.id.tv_detail_line, lastPosition == 0)
                        .setVisible(R.id.tv_comments_line, lastPosition == 1)
                        .setOnClickListener(R.id.img_finish, new View.OnClickListener()//关闭
                        {
                            @Override
                            public void onClick(View v)
                            {
                                doFinish();
                            }
                        }).setOnClickListener(R.id.tv_detail, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if (lastPosition == 0) return;
                        lastPosition = 0;
                        notifyDataSetChanged();
                        if (mServiceAdapter != null)
                        {
                            mServiceAdapter.setLastPosition(0);
                        }
                    }
                }).setOnClickListener(R.id.tv_comments, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if (lastPosition == 1) return;
                        lastPosition = 1;
                        notifyDataSetChanged();
                        if (mServiceAdapter != null)
                        {
                            mServiceAdapter.setLastPosition(1);
                        }
                    }
                });
            }

            @Override
            protected int setLastPosition()
            {
                return 0;
            }
        };
        mAdapter.addAdapter(mTitleAdapter);
        mTitleAdapter.setData(mData);

        mServiceAdapter = new BaseDelegateAdapter<CompanyServiceBean>(this, R.layout.layout_recyclerview)
        {
            private BaseAdapter mChild = null;

            @Override
            public void convert(BaseViewHolder holder, int position, CompanyServiceBean data)
            {
                RecyclerView recyclerView = holder.getView(R.id.layout_content);
                LinearLayoutManager manager = new LinearLayoutManager(mActivity);
                manager.setInitialPrefetchItemCount(3);
                recyclerView.setLayoutManager(manager);
                ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
                mChild = lastPosition == 0 ? new BaseAdapter<CompanyServiceBean.ServiceData>(mActivity, R.layout.item_service_detail_introduce)
                {
                    @Override
                    public void convert(BaseViewHolder holder, int position, CompanyServiceBean.ServiceData service)
                    {
                        holder.setText(service.getTitle(), R.id.item_title)
                                .setVisible(R.id.item_descript, !(position == 0));
                        if (position == 0)
                        {
                            ViewStub stub = holder.getView(R.id.item_stub);
                            stub.setLayoutResource(R.layout.layout_company_process);
                            stub.inflate();
                        } else
                        {
                            holder.setText(service.getDescript(), R.id.item_descript);
                        }

                    }
                } : new BaseAdapter<LawyerCommentBean>(mActivity, R.layout.layout_lawyer_comment)
                {
                    @Override
                    public void convert(BaseViewHolder holder, int position, LawyerCommentBean comments)
                    {
                        holder.setCircleImageResource(comments.getAvartar(), R.dimen.dp_30, R.id.item_image)
                                .setText(comments.getNickName(), R.id.item_name)
                                .setText(comments.getContent(), R.id.item_descript);
                    }
                };
                mChild.bindRecyclerView(recyclerView);
                mChild.setData(lastPosition == 0 ? data.getServiceData() : data.getComments());
            }

            @Override
            protected int setLastPosition()
            {
                return 0;
            }
        };
        mAdapter.addAdapter(mServiceAdapter);
        mServiceAdapter.setData(mData);

    }


    @Override
    protected void initData()
    {
        super.initData();
        if (mData != null)
        {
            mPresenter.getCompanyDetailData(mData);
        }
    }

    @OnClick({R.id.tv_kefu, R.id.tv_action})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.tv_kefu:
                //TODO 客服
                break;
            case R.id.tv_action:
                break;
        }
    }


    @Override
    public void showData(Object o)
    {
        super.showData(o);
        showContent();
        if (o instanceof CompanyServiceBean)
        {
            mData = (CompanyServiceBean) o;
            mTitleAdapter.setData(mData);
            mServiceAdapter.setData(mData);
        }
    }
}
