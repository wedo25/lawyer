package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import com.alibaba.android.vlayout.layout.LinearLayoutHelper;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.wei.lawyer.R;
import com.wei.lawyer.model.ContractBean;
import com.wei.lawyer.net.contract.ContractDetailView;
import com.wei.lawyer.net.presenter.HomePresenter;
import com.wei.lawyer.ui.adapter.BaseAdapter;
import com.wei.lawyer.ui.adapter.BaseDelegateAdapter;
import com.wei.lawyer.ui.adapter.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/8 13:19
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class ContractDetailActivity extends RootActivity<HomePresenter> implements ContractDetailView
{
    @Bind(R.id.group)
    Group mGroup;
    @Bind(R.id.img_collect)
    ImageView mCollect;
    @Bind(R.id.tv_price)
    TextView mTvPrice;
    @Bind(R.id.tv_old)
    TextView mTvOld;
    @Bind(R.id.tv_point)
    TextView mTvPoint;
    @Bind(R.id.layout_content)
    RecyclerView mRecyclerView;
    @Bind(R.id.refresh)
    SmartRefreshLayout mRefresh;


    private DelegateAdapter mAdapter;

    private BaseDelegateAdapter mPictureAdapter, mServiceAdapter, mOtherAdapter;

    private ContractBean mData;


    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_contract_detail;
    }

    @Override
    protected void setStatusBarColor()
    {

    }

    @Override
    protected HomePresenter createPresenter()
    {
        return new HomePresenter(this, this);
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_contract));
        setRefreshListener();
        mData = getIntent().getParcelableExtra(ContractDetailActivity.class.getSimpleName());
        mGroup.setVisibility(View.GONE);

        VirtualLayoutManager manager = new VirtualLayoutManager(this);
        mRecyclerView.setLayoutManager(manager);
        //关闭动效提升效率
        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        mRecyclerView.setItemViewCacheSize(3);

        mAdapter = new DelegateAdapter(manager, false);
        mRecyclerView.setAdapter(mAdapter);

        initAdapter();
    }

    /*初始化适配器*/
    private void initAdapter()
    {
        //合同图片
        mPictureAdapter = new BaseDelegateAdapter<ContractBean>(this, R.layout.layout_contract_detail_picture)
        {
            @Override
            public void convert(BaseViewHolder holder, int position, final ContractBean data)
            {
                RecyclerView recyclerView = holder.getView(R.id.recyclerView);
                LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
                recyclerView.setLayoutManager(manager);

                BaseAdapter adapter = new BaseAdapter<String>(mActivity, R.layout.item_contract_detail_picture)
                {
                    @Override
                    public void convert(BaseViewHolder holder, final int position, String s)
                    {
                        holder.setImageResource(s, R.id.item_image)
                                .setOnClickListener(R.id.item_image, new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        List<LocalMedia> list = new ArrayList<>();
                                        for (String urls : data.getScreenshots())
                                        {
                                            LocalMedia media = new LocalMedia();
                                            media.setPath(urls);
                                            list.add(media);
                                        }
                                        PictureSelector.create(mActivity)
                                                .themeStyle(R.style.picture_default_style)
                                                .openExternalPreview(position, list);
                                    }
                                });
                    }
                };
                adapter.bindRecyclerView(recyclerView);
                adapter.setData(data.getScreenshots());
            }
        };
        mAdapter.addAdapter(mPictureAdapter);
        mPictureAdapter.setData(mData);

        //合同介绍
        mServiceAdapter = new BaseDelegateAdapter<ContractBean>(this, R.layout.layout_contract_detail_descript)
        {
            @Override
            public void convert(BaseViewHolder holder, int position, ContractBean data)
            {
                TextView name = holder.getView(R.id.layout_name).findViewById(R.id.tv_title);
                name.setText(data.getTitle());
                TextView read = holder.getView(R.id.layout_name).findViewById(R.id.tv_descript);
                read.setText(new StringBuilder().append(getString(R.string.tv_read)).append(data.getVisitCount()));

                TextView service = holder.getView(R.id.layout_service).findViewById(R.id.tv_title);
                service.setText(getString(R.string.tv_service_process));//服务流程

                TextView other = holder.getView(R.id.layout_other).findViewById(R.id.tv_title);
                other.setText(getString(R.string.tv_other_contract));//其他文书

                //合同描述
                holder.setText(data.getDescription(), R.id.item_descript);
            }
        };
        mAdapter.addAdapter(mServiceAdapter);
        mServiceAdapter.setData(mData);

        //底部标签
        mOtherAdapter = new BaseDelegateAdapter<ContractBean>(this, R.layout.layout_contract_detail_label)
        {
            @Override
            public void convert(BaseViewHolder holder, int position, ContractBean data)
            {
                RecyclerView recyclerView = holder.getView(R.id.recyclerView);
                FlexboxLayoutManager manager = new FlexboxLayoutManager(mActivity);
                recyclerView.setLayoutManager(manager);

                BaseAdapter adapter = new BaseAdapter<ContractBean.RelatedBean>(mActivity, R.layout.item_contract_detail_other)
                {
                    @Override
                    public void convert(BaseViewHolder holder, int position, final ContractBean.RelatedBean relate)
                    {
                        holder.setText(relate.getTitle(), R.id.item_title)
                                .itemView.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                ContractBean contract = new ContractBean();
                                contract.setID(relate.getID());
                                openActivity(ContractDetailActivity.class, contract);
                            }
                        });
                    }
                };
                recyclerView.setAdapter(adapter);
                adapter.setData(data.getRelated());
            }

            @Override
            public LayoutHelper onCreateLayoutHelper()
            {
                LinearLayoutHelper helper = new LinearLayoutHelper();
                helper.setBgColor(getResources().getColor(R.color.white));
                return helper;
            }
        };
        mAdapter.addAdapter(mOtherAdapter);
        mOtherAdapter.setData(mData);
    }

    /*设置刷新监听*/
    private void setRefreshListener()
    {
        mRefresh.setEnableLoadMore(false);
        mRefresh.setOnRefreshListener(new OnRefreshListener()
        {
            @Override
            public void onRefresh(RefreshLayout refresh)
            {
                initData();
            }
        });
    }

    @Override
    protected void initData()
    {
        super.initData();
        mPresenter.getContractDetail(mData.getID());
    }

    /*设置价格*/
    private void setPrice()
    {
        mTvPrice.setText(new StringBuilder(getString(R.string.tv_money)).append(mData.getPrice() / 100d));
        mTvPoint.setText("律币抵1元");
        mTvOld.setText(new StringBuilder(getString(R.string.tv_original_price)).append(mData.getOriginalPrice() / 100d));
    }


    @OnClick({R.id.img_collect, R.id.tv_action})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.img_collect://收藏
                boolean collection = mData.isCollection();
                if (collection)
                {
                    mPresenter.unCollection(mData.getID());
                } else
                {
                    mPresenter.collection(mData.getID());
                }
                break;
            case R.id.tv_action://支付
                alert(getString(R.string.alert_contract_buy_success));
                doFinish(200);
                break;
        }
    }


    @Override
    public void showContractDetail(ContractBean data)
    {
        this.mData = data;
        if (mCurrent == ERROR)
        {
            showContent();
        }
        mPictureAdapter.setData(mData);
        mServiceAdapter.setData(mData);
        mOtherAdapter.setData(mData);
        mGroup.setVisibility(View.VISIBLE);
        setPrice();
        mCollect.setImageResource(data.isCollection() ? R.mipmap.contract_detail_collect_2 :
                R.mipmap.contract_detail_collect_1);
        mRefresh.setNoMoreData(true);
        stopRefreshingOrLoadMore();
    }


    @Override
    public void showCollection()
    {
        alert(getString(R.string.alert_contract_collect_success));
        mData.setCollection(true);
        mCollect.setImageResource(R.mipmap.contract_detail_collect_2);
    }

    @Override
    public void showUnCollection()
    {
        alert(getString(R.string.alert_contract_uncollect_success));
        mData.setCollection(false);
        mCollect.setImageResource(R.mipmap.contract_detail_collect_1);
    }
}
