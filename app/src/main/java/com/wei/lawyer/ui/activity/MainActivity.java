package com.wei.lawyer.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wei.lawyer.App;
import com.wei.lawyer.R;
import com.wei.lawyer.model.ConfigBean;
import com.wei.lawyer.model.UserBean;
import com.wei.lawyer.net.presenter.UserPresenter;
import com.wei.lawyer.ui.fragment.ChatFragment;
import com.wei.lawyer.ui.fragment.FindFragment;
import com.wei.lawyer.ui.fragment.HomeFragment;
import com.wei.lawyer.ui.fragment.MeFragment;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/10/24 10:35
 * 邮箱：1070138445@qq.com
 * 功能：主页面
 */
public class MainActivity extends BaseActivity<UserPresenter>
{
    @Bind(R.id.img_home)
    ImageView mImgHome;
    @Bind(R.id.tv_home_text)
    TextView mTvHomeText;
    @Bind(R.id.img_find)
    ImageView mImgFind;
    @Bind(R.id.tv_find_text)
    TextView mTvFindText;
    @Bind(R.id.img_chat)
    ImageView mImgChat;
    @Bind(R.id.tv_chat_text)
    TextView mTvChatText;
    @Bind(R.id.img_me)
    ImageView mImgMe;
    @Bind(R.id.tv_me_text)
    TextView mTvMeText;

    private HomeFragment mHomeFragment;
    private FindFragment mFindFragment;
    private ChatFragment mChatFragment;
    private MeFragment mMeFragment;

    private FragmentManager mManager;
    private int lastPosition = -1;


    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_main;
    }

    @Override
    protected void setStatusBarColor()
    {
    }


    @Override
    protected UserPresenter createPresenter()
    {
        return new UserPresenter(this, this);
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        mManager = getSupportFragmentManager();
        showFragment(0);
    }

    @Override
    protected void initData()
    {
        super.initData();
        mPresenter.getConfig();
    }

    //设置底部标签
    private void setBottomIcon(int position)
    {
        if (lastPosition == position)
        {
            return;
        }
        mTvHomeText.setTextColor(getResources().getColor(R.color.textUnSelected));
        mImgHome.setImageResource(R.mipmap.icon_home_unselected);

        mTvFindText.setTextColor(getResources().getColor(R.color.textUnSelected));
        mImgFind.setImageResource(R.mipmap.icon_find_unselected);

        mTvChatText.setTextColor(getResources().getColor(R.color.textUnSelected));
        mImgChat.setImageResource(R.mipmap.icon_chat_unselected);

        mTvMeText.setTextColor(getResources().getColor(R.color.textUnSelected));
        mImgMe.setImageResource(R.mipmap.icon_me_unselected);

        switch (position)
        {
            case 0:
                mTvHomeText.setTextColor(getResources().getColor(R.color.color));
                mImgHome.setImageResource(R.mipmap.icon_home_selected);
                break;
            case 1:
                mTvFindText.setTextColor(getResources().getColor(R.color.color));
                mImgFind.setImageResource(R.mipmap.icon_find_selected);
                break;
            case 2:
                mTvChatText.setTextColor(getResources().getColor(R.color.color));
                mImgChat.setImageResource(R.mipmap.icon_chat_selected);
                break;
            case 3:
                mTvMeText.setTextColor(getResources().getColor(R.color.color));
                mImgMe.setImageResource(R.mipmap.icon_me_selected);
                break;
            default:
                break;
        }
    }

    //显示Fragment
    private void showFragment(int position)
    {
        if (lastPosition == position)
        {
            return;
        }
        FragmentTransaction ft = mManager.beginTransaction();
        hideFragment(ft);
        switch (position)
        {
            case 0:
                if (mHomeFragment != null)
                {
                    ft.show(mHomeFragment);
                } else
                {
                    mHomeFragment = new HomeFragment();
                    ft.add(R.id.layout, mHomeFragment);
                }
                break;
            case 1:
                if (mFindFragment != null)
                {
                    ft.show(mFindFragment);
                } else
                {
                    mFindFragment = new FindFragment();
                    ft.add(R.id.layout, mFindFragment);
                }
                break;
            case 2:
                if (mChatFragment != null)
                {
                    ft.show(mChatFragment);
                } else
                {
                    mChatFragment = new ChatFragment();
                    ft.add(R.id.layout, mChatFragment);
                }
                break;
            case 3:
                if (mMeFragment != null)
                {
                    ft.show(mMeFragment);
                } else
                {
                    mMeFragment = new MeFragment();
                    ft.add(R.id.layout, mMeFragment);
                }
                break;
            default:
                break;
        }
        ft.commit();
        setBottomIcon(position);
    }

    //隐藏Fragment
    private void hideFragment(FragmentTransaction ft)
    {
        if (mHomeFragment != null)
        {
            ft.hide(mHomeFragment);
        }
        if (mFindFragment != null)
        {
            ft.hide(mFindFragment);
        }
        if (mChatFragment != null)
        {
            ft.hide(mChatFragment);
        }
        if (mMeFragment != null)
        {
            ft.hide(mMeFragment);
        }
    }


    @OnClick({R.id.tv_home, R.id.tv_find, R.id.tv_chat, R.id.tv_me})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.tv_home:
                showFragment(0);
                break;
            case R.id.tv_find:
                showFragment(1);
                break;
            case R.id.tv_chat:
                showFragment(2);
                break;
            case R.id.tv_me:
                showFragment(3);
                break;
        }
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        UserBean.updateUserInfo();
    }


    @Override
    public void showData(Object o)
    {
        super.showData(o);
        if (o instanceof ConfigBean)
        {
            App.mConfig = (ConfigBean) o;
        }
    }
}
