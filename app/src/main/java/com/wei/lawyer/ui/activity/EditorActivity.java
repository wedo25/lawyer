package com.wei.lawyer.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.wei.lawyer.R;
import com.wei.lawyer.model.EditorDataBean;
import com.wei.lawyer.net.BasePresenter;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/1 13:12
 * 邮箱：1070138445@qq.com
 * 功能：编辑页面
 */
public class EditorActivity extends BaseActivity
{
    @Bind(R.id.ed_descript)
    EditText mEdDescript;

    private EditorDataBean mData;


    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_editor;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        mData = getIntent().getParcelableExtra(getClass().getSimpleName());
        setTitle(mData.getTitle());
        mEdDescript.setHint(mData.getHint());
        mEdDescript.setText(mData.getDescript());
    }


    @OnClick({R.id.tv_cancel, R.id.tv_action})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.tv_cancel:
                doFinish();
                break;
            case R.id.tv_action:
                String descript = mEdDescript.getText().toString();
                if (TextUtils.isEmpty(descript) && mData.getEditorType() == EditorDataBean.MODIFYNICK)
                {
                    alert(getString(R.string.toast_no_nick));
                    return;
                }
                Intent it = new Intent();
                it.putExtra(ModifyUserActivity.class.getSimpleName(), mEdDescript.getText().toString());
                setResult(RESULT_OK, it);
                doFinish(200);
                break;
        }
    }
}
