package com.wei.lawyer.ui.activity;

import android.Manifest;
import android.os.Bundle;

import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;
import com.wei.lawyer.utils.LogUtils;
import com.wei.lawyer.utils.RxUtils;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * 作者：赵若位
 * 时间：2018/10/24 10:35
 * 邮箱：1070138445@qq.com
 * 功能：启动页
 */
public class SplashActivity extends BaseActivity
{
    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_splash;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);

        RxPermissions permiss = new RxPermissions(this);
        permiss.request(Manifest.permission.READ_PHONE_STATE)
                .subscribe(new Consumer<Boolean>()
                {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception
                    {
                        openActivityDelay(1500);
                    }
                });

    }


    /*延迟启动*/
    protected void openActivityDelay(long delay)
    {
        Observable.timer(delay, TimeUnit.MILLISECONDS)
                .compose(RxUtils.<Long>rxObservableSchedlers())
                .compose(this.<Long>bindUntilEvent(ActivityEvent.DESTROY))
                .subscribe(new Consumer<Long>()
                {
                    @Override
                    public void accept(Long aLong) throws Exception
                    {
                        openActivity(MainActivity.class, null);
                        doFinish();
                    }
                });
    }
}
