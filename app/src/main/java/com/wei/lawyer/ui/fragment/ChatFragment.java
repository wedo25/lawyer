package com.wei.lawyer.ui.fragment;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;

/**
 * 作者：赵若位
 * 时间：2018/10/27 13:45
 * 邮箱：1070138445@qq.com
 * 功能：首页
 */
public class ChatFragment extends BaseFragment
{
    @Override
    protected int getLayoutResource()
    {
        return R.layout.fragment_chat;
    }

    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }
}
