package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;
import com.wei.lawyer.utils.ImageUtils;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/6 16:37
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class VipCenterActivity extends BaseActivity
{
    @Bind(R.id.img_user)
    ImageView mImgUser;
    @Bind(R.id.tv_user)
    TextView mTvUser;
    @Bind(R.id.tv_open)
    TextView mTvOpen;

    private ImageUtils mUtils;


    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_vip_center;
    }

    @Override
    protected void setStatusBarColor()
    {

    }

    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_vip_center));

        mUtils = new ImageUtils(this);
        mUtils.setCircleImageResource("https://m.360buyimg.com/babel/jfs/t1/9856/12/6040/100933/5bdfdd00E59a88f2e/c7f2df496ea71f15.jpg", R.dimen.dp_57, mImgUser);
        mTvUser.setText("赵若位");
        mTvOpen.setText("尚未开通");
    }

    @OnClick(R.id.tv_action)
    public void onViewClicked()
    {
        //开通

    }
}
