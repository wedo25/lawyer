package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;
import com.wei.lawyer.ui.adapter.BasePageAdapter;
import com.wei.lawyer.ui.fragment.HistoryConsultingFragment;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/3 21:48
 * 邮箱：1070138445@qq.com
 * 功能：历史咨询
 */
public class HistoryConsultingActivity extends BaseActivity
{
    @Bind(R.id.tv_quick)
    TextView mTvQuick;
    @Bind(R.id.tv_picture)
    TextView mTvPicture;
    @Bind(R.id.tv_phone)
    TextView mTvPhone;
    @Bind(R.id.tv_quick_line)
    TextView mTvQuickLine;
    @Bind(R.id.tv_picture_line)
    TextView mTvPictureLine;
    @Bind(R.id.tv_phone_line)
    TextView mTvPhoneLine;
    @Bind(R.id.viewPager)
    ViewPager mViewPager;

    private BasePageAdapter mAdapter;

    private int lastPosition = -1;

    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_history_consulting;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_history_consluting));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int i, float v, int i1)
            {

            }

            @Override
            public void onPageSelected(int i)
            {
                setFragment(i);
            }

            @Override
            public void onPageScrollStateChanged(int i)
            {

            }
        });
        mAdapter = new BasePageAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        setFragment(0);

        mAdapter.setData(HistoryConsultingFragment.getFragment("快速咨询"));
        mAdapter.setData(HistoryConsultingFragment.getFragment("图文咨询"));
        mAdapter.setData(HistoryConsultingFragment.getFragment("电话咨询"));
    }


    private void setFragment(int position)
    {
        if (lastPosition == position) return;
        mTvQuick.setTextColor(getResources().getColor(R.color.textUnSelected));
        mTvQuickLine.setVisibility(View.GONE);

        mTvPicture.setTextColor(getResources().getColor(R.color.textUnSelected));
        mTvPictureLine.setVisibility(View.GONE);

        mTvPhone.setTextColor(getResources().getColor(R.color.textUnSelected));
        mTvPhoneLine.setVisibility(View.GONE);
        switch (position)
        {
            case 0:
                mTvQuick.setTextColor(getResources().getColor(R.color.textTitle));
                mTvQuickLine.setVisibility(View.VISIBLE);
                break;
            case 1:
                mTvPicture.setTextColor(getResources().getColor(R.color.textTitle));
                mTvPictureLine.setVisibility(View.VISIBLE);
                break;
            case 2:
                mTvPhone.setTextColor(getResources().getColor(R.color.textTitle));
                mTvPhoneLine.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        mViewPager.setCurrentItem(position);
        lastPosition = position;
    }


    @OnClick({R.id.tv_quick, R.id.tv_picture, R.id.tv_phone})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.tv_quick:
                setFragment(0);
                break;
            case R.id.tv_picture:
                setFragment(1);
                break;
            case R.id.tv_phone:
                setFragment(2);
                break;
        }
    }
}
