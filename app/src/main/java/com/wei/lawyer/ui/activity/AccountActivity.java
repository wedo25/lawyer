package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/2 16:19
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class AccountActivity extends BaseActivity
{
    @Bind(R.id.tv_phone_text)
    TextView mTvPhoneText;
    @Bind(R.id.tv_qq_text)
    TextView mTvQqText;
    @Bind(R.id.tv_weixin_text)
    TextView mTvWeixinText;
    @Bind(R.id.tv_weibo_text)
    TextView mTvWeiboText;

    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_account;
    }

    @Override
    protected void setStatusBarColor()
    {

    }

    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_setting_bind));

    }


    @OnClick({R.id.tv_phone, R.id.tv_pass, R.id.tv_qq, R.id.tv_weixin, R.id.tv_weibo})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.tv_phone://修改手机号码
                openActivity(ModifyPhoneActivity.class, null);
                break;
            case R.id.tv_pass://修改密码
                openActivity(ModifyPasWordActivity.class, null);
                break;
            case R.id.tv_qq://绑定QQ
                break;
            case R.id.tv_weixin://绑定微信
                break;
            case R.id.tv_weibo://绑定微博
                break;
        }
    }
}
