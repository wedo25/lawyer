package com.wei.lawyer.ui.activity;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;

/**
 * 作者：赵若位
 * 时间：2018/11/1 12:21
 * 邮箱：1070138445@qq.com
 * 功能：投诉建议
 */
public class SuggestActivity extends BaseActivity
{
    //TODO 投诉建议
    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_suggest;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }
}
