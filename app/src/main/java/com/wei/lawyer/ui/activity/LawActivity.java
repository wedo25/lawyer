package com.wei.lawyer.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.wei.lawyer.R;
import com.wei.lawyer.model.LawBean;
import com.wei.lawyer.net.BasePresenter;
import com.wei.lawyer.ui.adapter.BaseDelegateAdapter;
import com.wei.lawyer.ui.adapter.BaseViewHolder;
import com.wei.lawyer.utils.StatusBarUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/6 9:01
 * 邮箱：1070138445@qq.com
 * 功能：法条搜索
 */
public class LawActivity extends BaseActivity
{
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Bind(R.id.refresh)
    SmartRefreshLayout mRefresh;
    @Bind(R.id.layout_finish)
    ImageView mLayoutFinish;
    @Bind(R.id.tv_search)
    TextView mTvSearch;
    @Bind(R.id.img_search)
    ImageView mImgSearch;
    @Bind(R.id.img_vip)
    ImageView mImgVip;
    @Bind(R.id.tv_vip_text)
    TextView mTvVipText;
    @Bind(R.id.mToolBar)
    ConstraintLayout mToolBar;


    private DelegateAdapter mAdapter;
    private BaseDelegateAdapter mTitleAdapter, mLawAdapter;


    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_law;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        StatusBarUtils.setTranslucentForImageView(mActivity, findViewById(R.id.mToolBar));

        VirtualLayoutManager manager = new VirtualLayoutManager(this);
        manager.setInitialPrefetchItemCount(5);
        mRecyclerView.setLayoutManager(manager);
        //关闭动效提升效率
        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        //Item高度固定，避免浪费资源
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(5);


        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            private int mDistanceY;

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState)
            {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                //滑动的距离
                mDistanceY += dy;
                //toolbar的高度
                int toolbarHeight = mToolBar.getBottom();
                //当滑动的距离 <= toolbar高度的时候，改变Toolbar背景色的透明度，达到渐变的效果
                boolean scroll = mDistanceY <= toolbarHeight;
                mToolBar.setBackgroundColor(getResources().getColor(scroll ? R.color.transparent : R.color.white));
                mLayoutFinish.setImageResource(scroll ? R.mipmap.white_back : R.mipmap.icon_back);
                mTvSearch.setTextColor(getResources().getColor(scroll ? R.color.textUnSelected : R.color.white));
                mTvSearch.setBackgroundResource(scroll ? R.drawable.shape_law_search_unselected : R.drawable.shape_law_search_selected);
                mImgSearch.setImageResource(scroll ? R.mipmap.search_search_icon : R.mipmap.search_search_icon_2);
                mImgVip.setImageResource(scroll ? R.mipmap.law_homepage_notvip : R.mipmap.law_homepage_notvip_change);
                mTvVipText.setTextColor(scroll ? Color.parseColor("#748293") : getResources().getColor(R.color.textUnSelected));
                if (scroll)
                {
                    StatusBarUtils.setTranslucentForImageView(mActivity,0, findViewById(R.id.mToolBar));
                }else
                {
                    StatusBarUtils.setColor(mActivity, getResources().getColor(R.color.white), 0);
                }
                StatusBarUtils.setLightMode(mActivity);
            }
        });

        mAdapter = new DelegateAdapter(manager, false);
        mRecyclerView.setAdapter(mAdapter);
        initAdapter();
    }

    /*初始化Adapter*/
    private void initAdapter()
    {
        mTitleAdapter = new BaseDelegateAdapter<String>(this, R.layout.layout_law)
        {
            @Override
            public void convert(BaseViewHolder holder, int position, String s)
            {
                holder.setText(getString(R.string.tv_law_latest), R.id.item_title)
                        .setVisible(R.id.item_more, false);
            }
        };
        mAdapter.addAdapter(mTitleAdapter);
        mTitleAdapter.setData("");

        mLawAdapter = new BaseDelegateAdapter<LawBean>(this, R.layout.item_law)
        {
            @Override
            public void convert(BaseViewHolder holder, int position, LawBean data)
            {
                holder.setText(data.getTitle(), R.id.item_title)
                        .setText(data.getAuthor(), R.id.item_author)
                        .setText(data.getTime(), R.id.item_time);
            }
        };
        mAdapter.addAdapter(mLawAdapter);
    }

    @Override
    protected void initData()
    {
        super.initData();
        List<LawBean> list = new ArrayList<>();
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        list.add(new LawBean());
        mLawAdapter.setData(list);

    }


    @OnClick({R.id.tv_vip, R.id.tv_search})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.tv_vip://Vip
                openActivity(VipCenterActivity.class, null);
                break;
            case R.id.tv_search://搜索
                openActivity(LawSearchActivity.class, null);
                break;
        }
    }
}
