package com.wei.lawyer.ui.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.wei.lawyer.ui.fragment.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/11/3 10:48
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class BasePageAdapter extends FragmentPagerAdapter
{
    private List<BaseFragment> mList;

    public BasePageAdapter(FragmentManager fm)
    {
        super(fm);
        mList = new ArrayList<>();
    }

    public void setData(List<BaseFragment> list)
    {
        mList.addAll(list == null ? new ArrayList<BaseFragment>() : list);
        notifyDataSetChanged();
    }

    public void setData(BaseFragment fragment)
    {
        if (fragment == null) return;
        mList.add(fragment);
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position)
    {
        return mList.get(position);
    }

    @Override
    public int getCount()
    {
        return mList.size();
    }
}
