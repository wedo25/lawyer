package com.wei.lawyer.ui.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.wei.lawyer.R;
import com.wei.lawyer.model.LawyerCommentBean;
import com.wei.lawyer.ui.activity.BaseActivity;

/**
 * 作者：赵若位
 * 时间：2018/12/10 22:30
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public abstract class LawyerCommentAdapter extends BaseDelegateAdapter<LawyerCommentBean>
{

    private static final int ERROR = 0x002;//错误页面标签
    private boolean isError = false;
    private View mErrorView = null;


    public LawyerCommentAdapter(BaseActivity activity)
    {
        super(activity, R.layout.layout_lawyer_comment);
    }


    public void setErrorView(View view)
    {
        if (view == null) return;
        this.mErrorView = view;
        notifyDataSetChanged();
    }

    public void setError(boolean error)
    {
        this.isError = error;
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position)
    {
        if (isError && mErrorView != null)
        {
            return ERROR;
        }
        return super.getItemViewType(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int position)
    {
        if (getItemViewType(position) == ERROR)
        {
            return BaseViewHolder.getViewHolder(mErrorView, mActivity);
        }
        return super.onCreateViewHolder(parent, position);
    }


    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position)
    {
        if (getItemViewType(position) == ERROR)
        {
            setErrorMsg(holder);
        }
        super.onBindViewHolder(holder, position);
    }


    @Override
    public int getItemCount()
    {
        if (isError && mErrorView != null)
        {
            return 1;
        }
        return super.getItemCount();
    }

    @Override
    public void convert(BaseViewHolder holder, int position, LawyerCommentBean data)
    {
        holder.setCircleImageResource(data.getAvartar(), R.dimen.dp_30, R.id.item_image)
                .setText(data.getNickName(), R.id.item_name)
                .setText(data.getContent(), R.id.item_descript);
    }


    public abstract void setErrorMsg(BaseViewHolder holder);

}
