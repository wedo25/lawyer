package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/2 21:29
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class ModifyPhoneActivity extends BaseActivity
{
    @Bind(R.id.et_phone)
    EditText mEtPhone;
    @Bind(R.id.tv_code)
    TextView mTvCode;
    @Bind(R.id.et_code)
    EditText mEtCode;

    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_modify_phone;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_modify_phone));
    }


    @OnClick({R.id.et_code, R.id.tv_action})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.et_code:
                break;
            case R.id.tv_action:
                break;
        }
    }
}
