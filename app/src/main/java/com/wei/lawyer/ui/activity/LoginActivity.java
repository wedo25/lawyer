package com.wei.lawyer.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.wei.lawyer.App;
import com.wei.lawyer.R;
import com.wei.lawyer.model.TokenBean;
import com.wei.lawyer.net.presenter.UserPresenter;
import com.wei.lawyer.utils.LogUtils;
import com.wei.lawyer.utils.StringUtils;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/10/27 14:35
 * 邮箱：1070138445@qq.com
 * 功能：登录...
 */
public class LoginActivity extends BaseActivity<UserPresenter> implements TextWatcher
{
    @Bind(R.id.et_phone)
    EditText mEtPhone;
    @Bind(R.id.et_passWord)
    EditText mEtPassWord;
    @Bind(R.id.tv_action)
    TextView mTvAction;


    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_login;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected UserPresenter createPresenter()
    {
        return new UserPresenter(this, this);
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        mEtPhone.addTextChangedListener(this);
        mEtPassWord.addTextChangedListener(this);
        mTvAction.setClickable(false);
    }

    @OnClick({R.id.tv_action, R.id.tv_forget, R.id.tv_register, R.id.img_weixin, R.id.img_qq, R.id.img_weibo})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.tv_action://登录
                /*判断当前状态是否符合登录状态*/
                String account = mEtPhone.getText().toString();
                if (TextUtils.isEmpty(account) || (!StringUtils.isMobile(account)))
                {
                    alert(getString(R.string.alert_enter_phone));
                    return;
                }

                String pass = mEtPassWord.getText().toString();
                if (TextUtils.isEmpty(pass) || pass.length() < 6)
                {
                    alert(getString(R.string.alert_enter_passWord));
                    return;
                }

                mPresenter.login(account, pass);
                break;
            case R.id.tv_forget://忘记密码:
                openActivity(ForgetActivity.class, null);
                break;
            case R.id.tv_register://注册
                openActivity(RegisterActivity.class, null);
                break;
            case R.id.img_weixin://微信
                alert(getString(R.string.alert_have_no_function));
                break;
            case R.id.img_qq://QQ:
                alert(getString(R.string.alert_have_no_function));
                break;
            case R.id.img_weibo://微博:
                alert(getString(R.string.alert_have_no_function));
                break;
            default:
                break;
        }
    }


    /*设置主按钮的功能和样式*/
    private void showActionBtn(boolean isClick)
    {
        if (mTvAction == null) return;
        mTvAction.setClickable(isClick);
        mTvAction.setBackgroundColor(getResources().getColor(isClick ? R.color.color : R.color.textBG));
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {

    }

    @Override
    public void afterTextChanged(Editable s)
    {
        String account = mEtPhone.getText().toString();
        String pass = mEtPassWord.getText().toString();
        //判断当前两个输入框是否均为空
        boolean isClick = ((!TextUtils.isEmpty(account)) && (!TextUtils.isEmpty(pass)));
        showActionBtn(isClick);
    }


    @Override
    public void showData(Object o)
    {
        super.showData(o);
        if (o instanceof TokenBean)
        {
            alert(getString(R.string.alert_login_success));
            TokenBean token = (TokenBean) o;
            token.insert();
            App.mToken = token.getToken();
            LogUtils.e("本地Token：" + token.getToken());
            openActivity(MainActivity.class, null);
            //登录成功返回的目标页面
            Intent target = getIntent().getParcelableExtra(LoginActivity.class.getSimpleName());
            if (target != null)
            {
                startActivity(target);
            }
        }
    }
}
