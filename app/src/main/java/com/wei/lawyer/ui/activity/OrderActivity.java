package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;
import com.wei.lawyer.ui.adapter.BasePageAdapter;
import com.wei.lawyer.ui.fragment.OrderFragment;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/3 10:16
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class OrderActivity extends BaseActivity
{
    @Bind(R.id.tv_ordering)
    TextView mTvOrdering;
    @Bind(R.id.tv_ordered)
    TextView mTvOrdered;
    @Bind(R.id.tv_ordering_line)
    TextView mTvOrderingLine;
    @Bind(R.id.tv_ordered_line)
    TextView mTvOrderedLine;
    @Bind(R.id.viewPager)
    ViewPager mViewPager;

    private BasePageAdapter mAdapter;

    /*标签*/
    private int lastPosition = -1;



    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_order;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }


    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_order));
        mAdapter = new BasePageAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        setFragment(0);
        mAdapter.setData(OrderFragment.getFragment("正在进行"));
        mAdapter.setData(OrderFragment.getFragment("已付定金"));
    }


    private void setFragment(int position)
    {
        if (lastPosition == position) return;

        mTvOrdered.setTextColor(getResources().getColor(R.color.textUnSelected));
        mTvOrderedLine.setVisibility(View.GONE);

        mTvOrdering.setTextColor(getResources().getColor(R.color.textUnSelected));
        mTvOrderingLine.setVisibility(View.GONE);
        switch (position)
        {
            case 0:
                mTvOrdering.setTextColor(getResources().getColor(R.color.textTitle));
                mTvOrderingLine.setVisibility(View.VISIBLE);
                break;
            case 1:
                mTvOrdered.setTextColor(getResources().getColor(R.color.textTitle));
                mTvOrderedLine.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        mViewPager.setCurrentItem(position);
        lastPosition = position;
    }

    @OnClick({R.id.tv_history, R.id.tv_ordering, R.id.tv_ordered})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.tv_history://历史纪录
                openActivity(HistoryOrderActivity.class, null);
                break;
            case R.id.tv_ordering://正在进行中
                setFragment(0);
                break;
            case R.id.tv_ordered://已付订单:
                setFragment(1);
                break;
            default:
                break;
        }
    }
}
