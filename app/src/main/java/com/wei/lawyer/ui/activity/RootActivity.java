package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.annotation.IntDef;
import androidx.annotation.LayoutRes;
import android.view.View;
import android.view.ViewGroup;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 作者：赵若位
 * 时间：2018/11/22 15:40
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public abstract class RootActivity<T extends BasePresenter> extends BaseActivity<T>
{
    /*主内容*/
    protected static final int CONTENT = 0x00;
    /*错误页面*/
    protected static final int ERROR = 0x01;

    private ViewGroup mContent, mParent;
    private View mError;

    @State
    protected int mCurrent = CONTENT;


    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        mContent = findViewById(R.id.layout_content);
        if (mContent == null)
        {
            throw new IllegalStateException("The subclass of RootActivity must contain a View named 'layout_content'!");
        }
        if (!(mContent.getParent() instanceof ViewGroup))
        {
            throw new IllegalStateException("layout_content's ParentView should be a ViewGroup.");
        }
        mParent = (ViewGroup) mContent.getParent();
    }

    @Override
    public void showContent()
    {
        super.showContent();
        if (mCurrent == CONTENT)
        {
            return;
        }
        hideCurrentView();
        mCurrent = CONTENT;
        mContent.setVisibility(View.VISIBLE);
    }


    @Override
    public void showLoading()
    {
        super.showLoading();

    }

    @Override
    public void dissLoading()
    {
        super.dissLoading();

    }

    @Override
    public void showError()
    {
        super.showError();
        if (mCurrent == ERROR)
        {
            return;
        }
        if (mError == null)
        {
            View.inflate(this, R.layout.layout_error, mParent);
            mError = mParent.findViewById(R.id.layout_error);
        }
        hideCurrentView();
        mCurrent = ERROR;
        mError.setVisibility(View.VISIBLE);
    }

    /*设置自定义错误布局*/
    protected void setErrorView(@LayoutRes int layout)
    {
        View.inflate(this, layout, mParent);
        mError = mParent.findViewById(R.id.layout_error);
    }

    /*隐藏不需要显示的布局*/
    private void hideCurrentView()
    {
        switch (mCurrent)
        {
            case CONTENT:
                mContent.setVisibility(View.GONE);
                break;
            case ERROR:
                mError.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }


    @IntDef({CONTENT, ERROR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface State
    {

    }
}
