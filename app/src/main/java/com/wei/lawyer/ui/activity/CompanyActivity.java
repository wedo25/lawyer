package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.view.View;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.wei.lawyer.R;
import com.wei.lawyer.model.BaseDataBean;
import com.wei.lawyer.model.CompanyServiceBean;
import com.wei.lawyer.net.presenter.HomePresenter;
import com.wei.lawyer.ui.adapter.BaseAdapter;
import com.wei.lawyer.ui.adapter.BaseDelegateAdapter;
import com.wei.lawyer.ui.adapter.BaseViewHolder;
import com.wei.lawyer.utils.StringUtils;

import java.util.List;

import butterknife.Bind;

/**
 * 作者：赵若位
 * 时间：2018/11/15 11:07
 * 邮箱：1070138445@qq.com
 * 功能：企业服务
 */
public class CompanyActivity extends RootActivity<HomePresenter>
{
    @Bind(R.id.refresh)
    SmartRefreshLayout mRefresh;
    @Bind(R.id.layout_content)
    RecyclerView mRecyclerView;

    private BaseAdapter mAdapter;

    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_company;
    }

    @Override
    protected void setStatusBarColor()
    {

    }

    @Override
    protected HomePresenter createPresenter()
    {
        return new HomePresenter(this, this);
    }


    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_home_company));
        setRefreshListener();
        LinearLayoutManager manager = new LinearLayoutManager(mActivity.getApplicationContext());
        manager.setInitialPrefetchItemCount(5);
        mRecyclerView.setLayoutManager(manager);
        //关闭动效提升效率
        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        //Item高度固定，避免浪费资源
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(5);


        mAdapter = new BaseAdapter<BaseDataBean<List<CompanyServiceBean>>>(this, R.layout.item_company)
        {
            @Override
            public void convert(BaseViewHolder holder, int position, final BaseDataBean<List<CompanyServiceBean>> data)
            {
                holder.setText(data.getMsg(), R.id.item_title);
                RecyclerView recyclerView = holder.getView(R.id.recyclerView);

                GridLayoutManager manager = new GridLayoutManager(mActivity, 2);
                recyclerView.setLayoutManager(manager);
                ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);//关闭动效
                BaseDelegateAdapter adapter = new BaseDelegateAdapter<CompanyServiceBean>(mActivity, R.layout.item_company_child)
                {
                    @Override
                    public void convert(final BaseViewHolder holder, int position, final CompanyServiceBean child)
                    {
                        boolean include = StringUtils.isInclude(child.getTitle(), getString(R.string.tv_vip));
                        holder.setImageResource(child.getListView(), R.id.item_image)
                                .setText(include ? child.getTitle().replace(getString(R.string.tv_vip), "") : child.getTitle(), R.id.item_title)
                                .setText(child.getDescription(), R.id.item_descript)
                                .setVisible(R.id.item_descript, include ? View.INVISIBLE : View.VISIBLE)
                                .setText(child.getDescription(), R.id.item_label)
                                .setVisible(R.id.item_label, include)
                                .setVisible(R.id.item_vip, include)
                                .itemView.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                if (!mActivity.isFastDoubleClick())
                                {
                                    child.setService(data.getMsg());
                                    openActivity(CompanyDetailActivity.class, child);
                                }
                            }
                        });
                    }
                };
                recyclerView.setAdapter(adapter);
                adapter.setData(data.getT());
            }
        };
        mRecyclerView.setAdapter(mAdapter);
    }

    /*设置刷新监听*/
    private void setRefreshListener()
    {
        mRefresh.setEnableLoadMore(false);
        mRefresh.setOnRefreshListener(new OnRefreshListener()
        {
            @Override
            public void onRefresh(RefreshLayout refreshLayout)
            {
                mRefresh.finishRefresh();
                mPresenter.getCompanyData();
            }
        });
    }


    @Override
    protected void initData()
    {
        super.initData();
        mPresenter.getCompanyData();
    }


    @Override
    public void showData(Object o)
    {
        super.showData(o);
        showContent();
        if (o instanceof List)
        {
            List<BaseDataBean<List<CompanyServiceBean>>> list = (List<BaseDataBean<List<CompanyServiceBean>>>) o;
            mAdapter.setData(list);
        }
    }
}
