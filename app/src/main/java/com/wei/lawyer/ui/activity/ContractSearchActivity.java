package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.google.android.flexbox.FlexboxLayoutManager;
import com.wei.lawyer.R;
import com.wei.lawyer.model.ContractBean;
import com.wei.lawyer.model.ContractSearchBean;
import com.wei.lawyer.net.contract.ContractSearchView;
import com.wei.lawyer.net.presenter.HomePresenter;
import com.wei.lawyer.ui.adapter.BaseAdapter;
import com.wei.lawyer.ui.adapter.BaseViewHolder;
import com.wei.lawyer.ui.adapter.ContractListAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/8 17:12
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class ContractSearchActivity extends BaseActivity<HomePresenter> implements ContractSearchView
{
    @Bind(R.id.et_law)
    EditText mEtLaw;
    @Bind(R.id.layout_content)
    RecyclerView mHistory;
    @Bind(R.id.recyclerView)
    RecyclerView mSearch;

    private BaseAdapter mRecommentAdapter, mSearchAdapter;

    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_contract_search;
    }

    @Override
    protected void setStatusBarColor()
    {

    }

    @Override
    protected HomePresenter createPresenter()
    {
        return new HomePresenter(this, this);
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        mEtLaw.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                String descript = mEtLaw.getText().toString();
                mHistory.setVisibility(TextUtils.isEmpty(descript) ? View.VISIBLE : View.GONE);
                mSearch.setVisibility(TextUtils.isEmpty(descript) ? View.GONE : View.VISIBLE);
            }
        });
        initRecommentAdapter();

        initSearchAdapter();
    }

    /*初始化推荐适配器*/
    private void initRecommentAdapter()
    {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setInitialPrefetchItemCount(3);
        mHistory.setLayoutManager(manager);
        ((SimpleItemAnimator) mHistory.getItemAnimator()).setSupportsChangeAnimations(false);
        mHistory.setItemViewCacheSize(3);

        mRecommentAdapter = new BaseAdapter<ContractSearchBean>(this, R.layout.item_contract_search)
        {
            @Override
            public void convert(BaseViewHolder holder, final int position, final ContractSearchBean data)
            {
                holder.setText(data.getTitle(), R.id.item_title)
                        .setVisible(R.id.item_delete, position == 1);
                RecyclerView recyclerView = holder.getView(R.id.recyclerView);

                FlexboxLayoutManager managers = new FlexboxLayoutManager(mActivity);
                recyclerView.setLayoutManager(managers);
                BaseAdapter adapter = new BaseAdapter<ContractBean>(mActivity, R.layout.item_contract_detail_other)
                {
                    @Override
                    public void convert(BaseViewHolder holder, final int index, final ContractBean bean)
                    {
                        holder.setText(bean.getTitle(), R.id.item_title)
                                .itemView.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                if (position == 0)
                                {
                                    openActivity(ContractDetailActivity.class, bean);
                                } else
                                {
                                    mEtLaw.setText(bean.getTitle());
                                }
                            }
                        });
                    }
                };
                adapter.bindRecyclerView(recyclerView);
                adapter.setData(data.getList());
            }
        };
        mRecommentAdapter.bindRecyclerView(mHistory);
    }

    /*初始化搜索列表适配器*/
    private void initSearchAdapter()
    {
        GridLayoutManager manager = new GridLayoutManager(this, 2);
        manager.setInitialPrefetchItemCount(3);
        mSearch.setLayoutManager(manager);
        ((SimpleItemAnimator) mSearch.getItemAnimator()).setSupportsChangeAnimations(false);
        mSearch.setItemViewCacheSize(3);
        mSearchAdapter = new ContractListAdapter(mActivity);
        mSearchAdapter.bindRecyclerView(mSearch);
    }

    @Override
    protected void initData()
    {
        super.initData();
        mPresenter.getRecommentContract();
    }

    @OnClick(R.id.tv_search)
    public void onViewClicked()
    {
        String descript = mEtLaw.getText().toString();
        if (TextUtils.isEmpty(descript))
        {
            alert(getString(R.string.et_enter_key));
            return;
        }
        mPresenter.getContractSearchData(descript);
    }


    @Override
    public void showRecommentContract(List<ContractSearchBean> list)
    {
        mRecommentAdapter.setData(list);
    }

    @Override
    public void showHistoryContract(List<ContractBean> list)
    {
        List<ContractSearchBean> data = mRecommentAdapter.getData();
        if (data != null && data.size() != 0)
        {
            ContractSearchBean search = data.get(data.size() - 1);
            search.setList(list);
            mRecommentAdapter.notifyItemChanged(data.size() - 1);
        }
    }

    @Override
    public void showSearchContract(List<ContractBean> list)
    {
        //本地数据库插入搜索记录
        ContractBean data = new ContractBean();
        data.setTitle(mEtLaw.getText().toString());
        data.insertOrReplace();
        //刷新搜索数据
        mSearchAdapter.setData(list);
    }
}
