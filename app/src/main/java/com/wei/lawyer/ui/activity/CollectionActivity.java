package com.wei.lawyer.ui.activity;

import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;
import com.wei.lawyer.ui.adapter.BasePageAdapter;
import com.wei.lawyer.ui.fragment.CollectionFragment;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * 作者：赵若位
 * 时间：2018/11/6 8:27
 * 邮箱：1070138445@qq.com
 * 功能：我的收藏
 */
public class CollectionActivity extends BaseActivity
{
    @Bind(R.id.tv_law)
    TextView mTvLaw;
    @Bind(R.id.tv_template)
    TextView mTvTemplate;
    @Bind(R.id.tv_law_line)
    TextView mTvLawLine;
    @Bind(R.id.tv_template_line)
    TextView mTvTemplateLine;
    @Bind(R.id.viewPager)
    ViewPager mViewPager;

    private BasePageAdapter mAdapter;
    private int lastPosition = -1;


    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_collection;
    }

    @Override
    protected void setStatusBarColor()
    {

    }


    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }

    @Override
    protected void initView(Bundle savedInstanceState)
    {
        super.initView(savedInstanceState);
        setLeftBack();
        setTitle(getString(R.string.tv_collection));

        mAdapter = new BasePageAdapter(getSupportFragmentManager());
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int i, float v, int i1)
            {

            }

            @Override
            public void onPageSelected(int i)
            {
                setFragment(i);
            }

            @Override
            public void onPageScrollStateChanged(int i)
            {

            }
        });
        mViewPager.setAdapter(mAdapter);
        mAdapter.setData(CollectionFragment.getFragment(getString(R.string.tv_collection_law)));
        mAdapter.setData(CollectionFragment.getFragment(getString(R.string.tv_collection_template)));
        setFragment(0);
    }


    private void setFragment(int position)
    {
        if (lastPosition == position) return;
        mTvLaw.setTextColor(getResources().getColor(R.color.textUnSelected));
        mTvLawLine.setVisibility(View.GONE);

        mTvTemplate.setTextColor(getResources().getColor(R.color.textUnSelected));
        mTvTemplateLine.setVisibility(View.GONE);

        switch (position)
        {
            case 0:
                mTvLaw.setTextColor(getResources().getColor(R.color.textTitle));
                mTvLawLine.setVisibility(View.VISIBLE);
                break;
            case 1:
                mTvTemplate.setTextColor(getResources().getColor(R.color.textTitle));
                mTvTemplateLine.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
        mViewPager.setCurrentItem(position);
        lastPosition = position;
    }


    @OnClick({R.id.tv_law, R.id.tv_template})
    public void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.tv_law:
                setFragment(0);
                break;
            case R.id.tv_template:
                setFragment(1);
                break;
        }
    }
}
