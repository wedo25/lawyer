package com.wei.lawyer.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.net.BasePresenter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 作者：赵若位
 * 时间：2018/11/6 8:44
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class CollectionFragment extends BaseFragment
{
    @Bind(R.id.tv)
    TextView mTv;


    public static BaseFragment getFragment(String msg)
    {
        CollectionFragment fragment = new CollectionFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CollectionFragment.class.getSimpleName(), msg);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutResource()
    {
        return R.layout.fragment_collection;
    }

    @Override
    protected BasePresenter createPresenter()
    {
        return null;
    }


    @Override
    protected void initView()
    {
        super.initView();
        mTv.setText(getArguments().getString(this.getClass().getSimpleName()));
    }

}
