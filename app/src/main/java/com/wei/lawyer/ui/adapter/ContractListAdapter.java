package com.wei.lawyer.ui.adapter;

import android.content.Intent;
import android.view.View;

import com.wei.lawyer.R;
import com.wei.lawyer.model.ContractBean;
import com.wei.lawyer.ui.activity.BaseActivity;
import com.wei.lawyer.ui.activity.ContractDetailActivity;

/**
 * 作者：赵若位
 * 时间：2018/12/19 17:17
 * 邮箱：1070138445@qq.com
 * 功能：合同列表适配器
 */
public class ContractListAdapter extends BaseAdapter<ContractBean>
{

    public ContractListAdapter(BaseActivity activity)
    {
        super(activity, R.layout.item_contract);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, final ContractBean data)
    {
        holder.setImageResource(data.getListView(), R.id.item_image)
                .setText(data.getTitle(), R.id.item_title)
                .setText(new StringBuilder().append(mResources.getString(R.string.tv_money))
                        .append(data.getPrice() / 100d), R.id.item_price)
                .itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent it = new Intent(mActivity, ContractDetailActivity.class);
                it.putExtra(ContractDetailActivity.class.getSimpleName(), data);
                mActivity.startActivity(it);
                mActivity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
            }
        });
    }
}
