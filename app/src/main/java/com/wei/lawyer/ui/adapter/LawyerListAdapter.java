package com.wei.lawyer.ui.adapter;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.wei.lawyer.R;
import com.wei.lawyer.model.LawyerBean;
import com.wei.lawyer.ui.activity.BaseActivity;
import com.wei.lawyer.ui.activity.LawyerActivity;
import com.wei.lawyer.utils.RxUtils;

import java.util.Arrays;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Consumer;

/**
 * 作者：赵若位
 * 时间：2018/11/29 0:03
 * 邮箱：1070138445@qq.com
 * 功能：律师列表适配器
 */
public class LawyerListAdapter extends BaseAdapter<LawyerBean>
{

    public LawyerListAdapter(BaseActivity activity)
    {
        super(activity, R.layout.item_find_lawyer);
    }

    @Override
    public void convert(BaseViewHolder holder, int position, final LawyerBean data)
    {
        String[] label = data.getLabel();
        holder.setText(data.getNickName(), R.id.item_name)//律师名字
                .setText(TextUtils.concat(mResources.getString(R.string.tv_worktime), data.getCareerTime()).toString(), R.id.item_workTime)//工作年限
                .setText(data.getWorkAddress(), R.id.item_address)//工作地址
                .setText(TextUtils.concat(mResources.getString(R.string.tv_money), data.getWorkPrice(), mResources.getString(R.string.tv_count)).toString(), R.id.item_price)//服务价格
                .setText(data.getWorkType(), R.id.item_type)//职业类型
                .setText(data.getStatus() == 2 ? mResources.getString(R.string.tv_online) : mResources.getString(R.string.tv_outLine), R.id.item_onLine)//当前是否在线
                .setCircleImageResource(data.getAvatar(), R.dimen.dp_58, R.id.item_heade)
                .itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent it = new Intent(mActivity, LawyerActivity.class);
                it.putExtra(LawyerActivity.class.getSimpleName(), data);
                mActivity.startActivity(it);
            }
        });//律师头像

        /*设置律师在线状态*/
        setLawyerStatus((TextView) holder.getView(R.id.item_onLine), data);

        /*设置律师擅长的标签*/
        RecyclerView child = holder.getView(R.id.item_label);
        child.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
        BaseAdapter<String> adapter = new BaseAdapter<String>(mActivity, R.layout.item_find_lawyer_label)
        {
            @Override
            public void convert(BaseViewHolder holder, int position, String s)
            {
                holder.setText(s, R.id.item_title);
            }
        };
        adapter.bindRecyclerView(child);
        adapter.setData((label == null || label.length == 0) ? null : Arrays.asList(label));
    }


    //设置律师在线状态
    private void setLawyerStatus(final TextView status, final LawyerBean data)
    {
        Observable.create(new ObservableOnSubscribe<Drawable>()
        {
            @Override
            public void subscribe(ObservableEmitter<Drawable> e) throws Exception
            {
                if (data == null)
                {
                    Observable.empty();
                } else
                {
                    Drawable drawable = mResources.getDrawable(data.getStatus() == 2 ? R.mipmap.lawyer_online : R.mipmap.lawyer_busy);
                    drawable.setBounds(0, 0,
                            mResources.getDimensionPixelOffset(R.dimen.dp_11),
                            mResources.getDimensionPixelOffset(R.dimen.dp_11));
                    e.onNext(drawable);
                    e.onComplete();
                }
            }
        }).compose(RxUtils.<Drawable>rxObservableSchedlers())
                .subscribe(new Consumer<Drawable>()
                {
                    @Override
                    public void accept(Drawable drawable) throws Exception
                    {
                        status.setCompoundDrawables(drawable, null, null, null);
                    }
                });
    }
}
