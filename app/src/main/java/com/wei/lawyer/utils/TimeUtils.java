package com.wei.lawyer.utils;


import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 作者：赵若位
 * 时间：2018/12/7 18:09
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class TimeUtils
{


    public static String getServiceTime(String time)
    {
        if (TextUtils.isEmpty(time)) return null;
        time = time.replace("Z", " UTC");//UTC是本地时间
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        Date d = null;
        try
        {
            d = format.parse(time);
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sf.format(d);
    }
}
