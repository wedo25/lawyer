package com.wei.lawyer.utils;

import android.app.Dialog;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wei.lawyer.R;
import com.wei.lawyer.model.GiftBean;
import com.wei.lawyer.ui.activity.BaseActivity;
import com.wei.lawyer.ui.adapter.BaseAdapter;
import com.wei.lawyer.ui.adapter.BaseViewHolder;

/**
 * 作者：赵若位
 * 时间：2018/12/13 11:01
 * 邮箱：1070138445@qq.com
 * 功能：律师详情页赠送礼物弹窗
 */
public class GiftDialog extends Dialog
{
    private TextView mGift;
    private EditText mEditText;

    private OnClickGiftListener mListener;


    public GiftDialog(BaseActivity activity)
    {
        super(activity, R.style.DialogStyle);
        initView(activity);
    }


    private void initView(BaseActivity activity)
    {
        View layout = LayoutInflater.from(activity.getApplicationContext()).inflate(R.layout.dialog_gift, null);
        RecyclerView recyclerView = layout.findViewById(R.id.recyclerView);
        mGift = layout.findViewById(R.id.tv_gift);
        mEditText = layout.findViewById(R.id.ed_gift);
        recyclerView.setLayoutManager(new GridLayoutManager(activity, 3));
        BaseAdapter<GiftBean> adapter = new BaseAdapter<GiftBean>(activity, R.layout.item_gift)
        {
            @Override
            public void convert(BaseViewHolder holder, int position, final GiftBean data)
            {
                holder.setImageResource(data.getResource(), R.id.item_image)
                        .itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if (mListener != null)
                        {
                            mListener.onClickGiftListener(data.getPrice());
                        }
                        dismiss();
                    }
                });
            }
        };
        recyclerView.setAdapter(adapter);
        adapter.setData(GiftBean.getGiftList());
        /*输入金额提交判断*/
        mGift.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*非空判断并提示*/
                String price = mEditText.getText().toString();
                if (TextUtils.isEmpty(price))
                {
                    Toast.makeText(getContext(),
                            getContext().getString(R.string.alert_please_enter_price), Toast.LENGTH_SHORT).show();
                    return;
                }
                /*非零判断并提示*/
                Integer amount = Integer.valueOf(price);
                if (amount == 0)
                {
                    Toast.makeText(getContext(),
                            getContext().getString(R.string.alert_enter_than_one), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mListener != null)
                {
                    mListener.onClickGiftListener(amount);
                }
                dismiss();
            }
        });

        setContentView(layout);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
    }


    public GiftDialog setOnClickGiftListener(OnClickGiftListener listener)
    {
        this.mListener = listener;
        return this;
    }


    public interface OnClickGiftListener
    {
        void onClickGiftListener(int price);
    }
}
