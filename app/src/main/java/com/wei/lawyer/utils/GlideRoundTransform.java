package com.wei.lawyer.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import androidx.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.wei.lawyer.R;

import java.security.MessageDigest;

/**
 * 作者：赵若位
 * 时间：2018/11/14 19:03
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class GlideRoundTransform extends BitmapTransformation
{
    private float mRound = 0.0f;

    public GlideRoundTransform(Context context)
    {
        this(context, context.getResources().getDimension(R.dimen.dp_4));
    }

    public GlideRoundTransform(Context context, float round)
    {
        super();
        this.mRound = round;
    }


    @Override
    protected Bitmap transform(@NonNull BitmapPool pool, @NonNull Bitmap source, int outWidth, int outHeight)
    {
        if (source == null) return null;

        Bitmap result = pool.get(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
        if (result == null)
        {
            result = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();
        paint.setShader(new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
        paint.setAntiAlias(true);
        RectF rectF = new RectF(0f, 0f, source.getWidth(), source.getHeight());
        canvas.drawRoundRect(rectF, mRound, mRound, paint);
        return result;
    }

    @Override
    public void updateDiskCacheKey(@NonNull MessageDigest messageDigest)
    {

    }
}
