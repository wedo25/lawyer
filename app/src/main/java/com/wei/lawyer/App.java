package com.wei.lawyer;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshFooterCreater;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreater;
import com.scwang.smartrefresh.layout.api.RefreshFooter;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.wei.lawyer.model.ConfigBean;
import com.wei.lawyer.model.TokenBean;
import com.wei.lawyer.sql.DBManager;
import com.wei.lawyer.sql.DaoSession;
import com.wei.lawyer.sql.TokenBeanDao;
import com.wei.lawyer.utils.ActivityManager;
import com.wei.lawyer.utils.InitializeManager;
import com.wei.lawyer.utils.LogUtils;

import java.text.SimpleDateFormat;

/**
 * 作者：赵若位
 * 时间：2018/10/24 8:00
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class App extends Application
{
    private static Context mContext;
    public static DaoSession mSession;
    public static String mToken = null;//用户唯一标识
    public static ConfigBean mConfig = null;


    static
    {
        //设置全局的Header构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreator(new DefaultRefreshHeaderCreater()
        {
            @Override
            public RefreshHeader createRefreshHeader(Context context, RefreshLayout layout)
            {
                //全局设置主题颜色
                layout.setPrimaryColorsId(R.color.editBg, R.color.textUnSelected);
                //指定为经典Header，默认是 贝塞尔雷达Header
                return new ClassicsHeader(context).setTimeFormat(new SimpleDateFormat());
            }
        });

        //设置全局的Footer构建器
        SmartRefreshLayout.setDefaultRefreshFooterCreator(new DefaultRefreshFooterCreater()
        {
            @Override
            public RefreshFooter createRefreshFooter(Context context, RefreshLayout layout)
            {
                //指定为经典Footer，默认是 BallPulseFooter
                return new ClassicsFooter(context).setDrawableSize(20);
            }
        });
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        mContext = getApplicationContext();
        //初始化数据库
        mSession = DBManager.getManager(this.getApplicationContext()).init();
        InitializeManager.init(this);
        getToken();
    }


    public static Context getContext()
    {
        return mContext;
    }


    /*获取本地Token记录*/
    public static String getToken()
    {
        TokenBeanDao dao = mSession.getTokenBeanDao();
        TokenBean token = dao.queryBuilder().build().unique();
        mToken = token != null ? token.getToken() : null;
        LogUtils.e("本地Token：" + mToken);
        return mToken;
    }
}
