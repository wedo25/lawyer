package com.wei.lawyer.sql;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import com.wei.lawyer.model.ContractBean;
import com.wei.lawyer.model.LawHistoryBean;
import com.wei.lawyer.model.TokenBean;

import com.wei.lawyer.sql.ContractBeanDao;
import com.wei.lawyer.sql.LawHistoryBeanDao;
import com.wei.lawyer.sql.TokenBeanDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig contractBeanDaoConfig;
    private final DaoConfig lawHistoryBeanDaoConfig;
    private final DaoConfig tokenBeanDaoConfig;

    private final ContractBeanDao contractBeanDao;
    private final LawHistoryBeanDao lawHistoryBeanDao;
    private final TokenBeanDao tokenBeanDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        contractBeanDaoConfig = daoConfigMap.get(ContractBeanDao.class).clone();
        contractBeanDaoConfig.initIdentityScope(type);

        lawHistoryBeanDaoConfig = daoConfigMap.get(LawHistoryBeanDao.class).clone();
        lawHistoryBeanDaoConfig.initIdentityScope(type);

        tokenBeanDaoConfig = daoConfigMap.get(TokenBeanDao.class).clone();
        tokenBeanDaoConfig.initIdentityScope(type);

        contractBeanDao = new ContractBeanDao(contractBeanDaoConfig, this);
        lawHistoryBeanDao = new LawHistoryBeanDao(lawHistoryBeanDaoConfig, this);
        tokenBeanDao = new TokenBeanDao(tokenBeanDaoConfig, this);

        registerDao(ContractBean.class, contractBeanDao);
        registerDao(LawHistoryBean.class, lawHistoryBeanDao);
        registerDao(TokenBean.class, tokenBeanDao);
    }
    
    public void clear() {
        contractBeanDaoConfig.clearIdentityScope();
        lawHistoryBeanDaoConfig.clearIdentityScope();
        tokenBeanDaoConfig.clearIdentityScope();
    }

    public ContractBeanDao getContractBeanDao() {
        return contractBeanDao;
    }

    public LawHistoryBeanDao getLawHistoryBeanDao() {
        return lawHistoryBeanDao;
    }

    public TokenBeanDao getTokenBeanDao() {
        return tokenBeanDao;
    }

}
