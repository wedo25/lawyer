package com.wei.lawyer.sql;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

import com.wei.lawyer.model.TokenBean;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "TOKEN_BEAN".
*/
public class TokenBeanDao extends AbstractDao<TokenBean, String> {

    public static final String TABLENAME = "TOKEN_BEAN";

    /**
     * Properties of entity TokenBean.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Token = new Property(0, String.class, "token", true, "TOKEN");
    }


    public TokenBeanDao(DaoConfig config) {
        super(config);
    }
    
    public TokenBeanDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"TOKEN_BEAN\" (" + //
                "\"TOKEN\" TEXT PRIMARY KEY NOT NULL );"); // 0: token
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"TOKEN_BEAN\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, TokenBean entity) {
        stmt.clearBindings();
 
        String token = entity.getToken();
        if (token != null) {
            stmt.bindString(1, token);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, TokenBean entity) {
        stmt.clearBindings();
 
        String token = entity.getToken();
        if (token != null) {
            stmt.bindString(1, token);
        }
    }

    @Override
    public String readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0);
    }    

    @Override
    public TokenBean readEntity(Cursor cursor, int offset) {
        TokenBean entity = new TokenBean( //
            cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0) // token
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, TokenBean entity, int offset) {
        entity.setToken(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
     }
    
    @Override
    protected final String updateKeyAfterInsert(TokenBean entity, long rowId) {
        return entity.getToken();
    }
    
    @Override
    public String getKey(TokenBean entity) {
        if(entity != null) {
            return entity.getToken();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(TokenBean entity) {
        return entity.getToken() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
