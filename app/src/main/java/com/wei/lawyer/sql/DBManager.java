package com.wei.lawyer.sql;

import android.content.Context;

/**
 * 作者：赵若位
 * 时间：2018/5/6 10:13
 * 邮箱：1070138445@qq.com
 * 功能：数据库操作对象
 */

public class DBManager
{
    private static DBManager mManager;
    private DaoSession mSession;
    private Context mContext;


    private DBManager(Context context)
    {
        this.mContext = context;
    }

    public static DBManager getManager(Context context)
    {
        if (mManager == null)
        {
            synchronized (DBManager.class)
            {
                if (mManager == null)
                {
                    mManager = new DBManager(context);
                }
            }
        }
        return mManager;
    }

    /**
     * 初始化DaoSession
     *
     * @return
     */
    public DaoSession init()
    {
        if (mSession == null)
        {
            String fileName = mContext.getPackageName();
            DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(mContext, fileName + "-db", null);
            DaoMaster master = new DaoMaster(helper.getWritableDatabase());
            mSession = master.newSession();
        }
        return mSession;
    }


}
