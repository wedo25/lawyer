package com.wei.lawyer.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * 作者：赵若位
 * 时间：2018/11/6 18:36
 * 邮箱：1070138445@qq.com
 * 功能：法条搜索历史纪录
 */
@Entity
public class LawHistoryBean
{
    /*搜索文字对象*/
    @Id
    private String mDescript;
    /*搜索时间*/
    private long mTime;


    public LawHistoryBean()
    {
    }

    public LawHistoryBean(String descript)
    {
        mDescript = descript;
        this.mTime = System.currentTimeMillis();
    }

    @Generated(hash = 1655858054)
    public LawHistoryBean(String mDescript, long mTime)
    {
        this.mDescript = mDescript;
        this.mTime = mTime;
    }

    public String getDescript()
    {
        return this.mDescript;
    }

    public void setDescript(String mDescript)
    {
        this.mDescript = mDescript;
    }

    public long getTime()
    {
        return this.mTime;
    }

    public void setTime(long mTime)
    {
        this.mTime = mTime;
    }

    public String getMDescript() {
        return this.mDescript;
    }

    public void setMDescript(String mDescript) {
        this.mDescript = mDescript;
    }

    public long getMTime() {
        return this.mTime;
    }

    public void setMTime(long mTime) {
        this.mTime = mTime;
    }


}
