package com.wei.lawyer.model;

/**
 * 作者：赵若位
 * 时间：2018/10/31 17:41
 * 邮箱：1070138445@qq.com
 * 功能：法律小讲堂
 */
public class LawyerClassBean
{

    /**
     * ID : 64
     * Title : 没提前提辞职，公司要求给违约金合法吗？
     * Content : 公司要求违约金不合法。在法律上公司只能在劳动合同上就培训服务期和竞业限制约定违约金，其他约定违约金的事由不合法。 当然，劳动者没提前一个月辞职的，公司需要提供证据证明有受到损失，并要求该劳动者赔偿，而不是支付违约金。
     */

    private int ID;
    private String Title;
    private String Content;

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public String getTitle()
    {
        return Title;
    }

    public void setTitle(String Title)
    {
        this.Title = Title;
    }

    public String getContent()
    {
        return Content;
    }

    public void setContent(String Content)
    {
        this.Content = Content;
    }
}
