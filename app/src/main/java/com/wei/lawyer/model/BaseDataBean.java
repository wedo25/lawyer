package com.wei.lawyer.model;

import com.google.gson.annotations.SerializedName;

/**
 * 作者：赵若位
 * 时间：2018/10/24 11:56
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class BaseDataBean<T>
{
    @SerializedName("Code")
    private int code;

    @SerializedName("Message")
    private String msg;

    @SerializedName("Data")
    private T mT;

    public BaseDataBean()
    {
    }

    public BaseDataBean(String msg, T t)
    {
        this.msg = msg;
        mT = t;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public T getT()
    {
        return mT;
    }

    public void setT(T t)
    {
        mT = t;
    }
}
