package com.wei.lawyer.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 作者：赵若位
 * 时间：2018/11/1 11:54
 * 邮箱：1070138445@qq.com
 * 功能：打开网页传递地址的实体对象
 */
public class WebDataBean  implements Parcelable
{
    private String mTitle;
    private String mUrl;
    private String mDescript;

    public WebDataBean()
    {
    }


    protected WebDataBean(Parcel in)
    {
        mTitle = in.readString();
        mUrl = in.readString();
        mDescript = in.readString();
    }

    public static final Creator<WebDataBean> CREATOR = new Creator<WebDataBean>()
    {
        @Override
        public WebDataBean createFromParcel(Parcel in)
        {
            return new WebDataBean(in);
        }

        @Override
        public WebDataBean[] newArray(int size)
        {
            return new WebDataBean[size];
        }
    };

    public String getTitle()
    {
        return mTitle;
    }

    public void setTitle(String title)
    {
        mTitle = title;
    }

    public String getUrl()
    {
        return mUrl;
    }

    public void setUrl(String url)
    {
        mUrl = url;
    }

    public String getDescript()
    {
        return mDescript;
    }

    public void setDescript(String descript)
    {
        mDescript = descript;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(mTitle);
        dest.writeString(mUrl);
        dest.writeString(mDescript);
    }
}
