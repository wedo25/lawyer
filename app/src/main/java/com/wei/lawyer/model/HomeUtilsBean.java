package com.wei.lawyer.model;

import com.wei.lawyer.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/10/31 17:45
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class HomeUtilsBean
{
    private int imageResource;
    private String title;
    private String descript;


    public static List<HomeUtilsBean> getList()
    {
        List<HomeUtilsBean> list = new ArrayList<>();
        list.add(new HomeUtilsBean(R.mipmap.tool_interest, "利息", "分段计算"));
        list.add(new HomeUtilsBean(R.mipmap.tool_delay_interest, "延迟利息", "分类计算"));
        list.add(new HomeUtilsBean(R.mipmap.tool_tax, "个税", "工资薪资"));
        list.add(new HomeUtilsBean(R.mipmap.tool_tax_other, "个税", "其他项目"));
        list.add(new HomeUtilsBean(R.mipmap.tool_cost, "诉讼费", "案件申请"));
        list.add(new HomeUtilsBean(R.mipmap.tool_cost_2, "诉讼费", "案件受理"));
        return list;
    }


    public HomeUtilsBean(int imageResource, String title, String descript)
    {
        this.imageResource = imageResource;
        this.title = title;
        this.descript = descript;
    }

    public int getImageResource()
    {
        return imageResource;
    }

    public void setImageResource(int imageResource)
    {
        this.imageResource = imageResource;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescript()
    {
        return descript;
    }

    public void setDescript(String descript)
    {
        this.descript = descript;
    }
}
