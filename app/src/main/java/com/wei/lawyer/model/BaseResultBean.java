package com.wei.lawyer.model;

import com.google.gson.annotations.SerializedName;

/**
 * 作者：赵若位
 * 时间：2018/11/28 17:11
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class BaseResultBean<T>
{
    @SerializedName("Code")
    private int code;

    @SerializedName("Message")
    private String msg;

    @SerializedName("Data")
    private DataBean<T> mData;

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public DataBean<T> getData()
    {
        return mData;
    }

    public void setData(DataBean<T> data)
    {
        mData = data;
    }

    public static class DataBean<T>
    {
        @SerializedName("TotalCount")
        private int mTotal;
        @SerializedName("Result")
        private T mT;

        public int getTotal()
        {
            return mTotal;
        }

        public void setTotal(int total)
        {
            mTotal = total;
        }

        public T getT()
        {
            return mT;
        }

        public void setT(T t)
        {
            mT = t;
        }
    }
}

