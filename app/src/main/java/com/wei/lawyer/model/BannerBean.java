package com.wei.lawyer.model;

/**
 * 作者：赵若位
 * 时间：2018/12/7 15:19
 * 邮箱：1070138445@qq.com
 * 功能：首页Banner
 */
public class BannerBean
{
    /**
     * ID : 241
     * ImageUrl : https://pub.cdn.lvlvlaw.com/1122zsm.png
     * Link : lvlv://mobile/jump?androidpage=ASDY29tLmx2bHZsYXcudWkuYWN0aXZpdHkuY29udmVyc2F0aW9uLkxhd3llckluZm9BY3Rpdml0eQ==&lawyerID=223627&a_s_id=223627&iospage=ASDTGF3eWVySW5mb1ZpZXdDb250cm9sbGVy
     * Text : 一 、律师广告位（付费）
     * Display : true
     * AdType : 1
     * Ordinal : 40
     * ChannelType : 1
     * UpdatedAt : 2018-11-22 18:00:11
     */

    private int ID;
    private String ImageUrl;
    private String Link;
    private String Text;
    private boolean Display;
    private int AdType;
    private int Ordinal;
    private int ChannelType;
    private String UpdatedAt;

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public String getImageUrl()
    {
        return ImageUrl;
    }

    public void setImageUrl(String ImageUrl)
    {
        this.ImageUrl = ImageUrl;
    }

    public String getLink()
    {
        return Link;
    }

    public void setLink(String Link)
    {
        this.Link = Link;
    }

    public String getText()
    {
        return Text;
    }

    public void setText(String Text)
    {
        this.Text = Text;
    }

    public boolean isDisplay()
    {
        return Display;
    }

    public void setDisplay(boolean Display)
    {
        this.Display = Display;
    }

    public int getAdType()
    {
        return AdType;
    }

    public void setAdType(int AdType)
    {
        this.AdType = AdType;
    }

    public int getOrdinal()
    {
        return Ordinal;
    }

    public void setOrdinal(int Ordinal)
    {
        this.Ordinal = Ordinal;
    }
    public int getChannelType()
    {
        return ChannelType;
    }

    public void setChannelType(int ChannelType)
    {
        this.ChannelType = ChannelType;
    }

    public String getUpdatedAt()
    {
        return UpdatedAt;
    }

    public void setUpdatedAt(String UpdatedAt)
    {
        this.UpdatedAt = UpdatedAt;
    }
}
