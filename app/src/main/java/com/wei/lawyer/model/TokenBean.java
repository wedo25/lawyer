package com.wei.lawyer.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.wei.lawyer.App;
import com.wei.lawyer.sql.DaoSession;
import com.wei.lawyer.sql.TokenBeanDao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * 作者：赵若位
 * 时间：2018/11/29 18:31
 * 邮箱：1070138445@qq.com
 * 功能：
 */
@Entity
public class TokenBean
{
    @Id
    @SerializedName("Token")
    private String token;

    @Generated(hash = 157022151)
    public TokenBean(String token)
    {
        this.token = token;
    }

    @Generated(hash = 1886787915)
    public TokenBean()
    {
    }

    public String getToken()
    {
        return this.token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }


    /*存储*/
    public void insert()
    {
        DaoSession session = App.mSession;
        if (session == null || TextUtils.isEmpty(token))
        {
            return;
        }
        TokenBeanDao dao = session.getTokenBeanDao();
        dao.deleteAll();
        dao.insert(this);
    }


    /*删除本地Token记录*/
    public static void delete()
    {
        DaoSession session = App.mSession;
        if (session == null) return;
        TokenBeanDao dao = session.getTokenBeanDao();
        dao.deleteAll();
    }
}
