package com.wei.lawyer.model;

/**
 * 作者：赵若位
 * 时间：2018/12/10 11:21
 * 邮箱：1070138445@qq.com
 * 功能：律师查询条件对象
 */
public class LawyerSearchBean
{

    private int mPage;
    private int mOrder;
    private String mTag;
    private String mLocation;


    public LawyerSearchBean()
    {
        this(1, 0, null, null);
    }

    public LawyerSearchBean(int page, int order, String tag, String location)
    {
        mPage = page;
        mOrder = order;
        mTag = tag;
        mLocation = location;
    }


    public int getPage()
    {
        return mPage;
    }

    public void setPage(int page)
    {
        mPage = page;
    }

    public int getOrder()
    {
        return mOrder;
    }

    public void setOrder(int order)
    {
        mOrder = order;
    }

    public String getTag()
    {
        return mTag;
    }

    public void setTag(String tag)
    {
        mTag = tag;
    }

    public String getLocation()
    {
        return mLocation;
    }

    public void setLocation(String location)
    {
        mLocation = location;
    }


    /*请求律师页面自增*/
    public void increase()
    {
        this.mPage += 1;
    }
}
