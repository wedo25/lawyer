package com.wei.lawyer.model;

import com.contrarywind.interfaces.IPickerViewData;

import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/12/17 10:51
 * 邮箱：1070138445@qq.com
 * 功能：文书服务实体对象
 */
public class BookBean
{
    /**
     * ID : 534
     * Title : 定制文书
     * Screenshot : https://og0f478en.qnssl.com/8d196f46-9a71-9154-addd-cc14ccc02e1a.png
     * ListView : https://og0f478en.qnssl.com/8d196f46-9a71-9154-addd-cc14ccc02e1a.png
     * OrderView : https://archives.cdn.lvlvlaw.com/a4f4f461-da4c-e2d3-834d-15b89bd66bd3.png
     * OriginalPrice : 3000
     * Price : 3000
     * Deduction : 0
     * Category : 200
     * VisitCount : 270839
     * SalesVolume : 2475
     * Content : 1、订金支付成功后，订金有效期为24小时； 2、具体文书价格请与律师商议，参考价格仅供参考，实际文书价格请以律师报价为准； 3、关于订金，如订单达成，订金在支付时自动抵扣。订单未达成，订金则作为前期服务费，不支持退款； 4、律师有权不回答与代写文书无关的法律问题。
     * PriceNote : 1. 具体文书定制价格由律师根据合同的难易程度及交付时间等，进行评估。双方达成定制订单的，订金可用于抵扣定制费用； 2. 定制服务不支持退款。
     * TimeNote : 温馨提示： 1. 订金可用于抵扣合同定制费用，因合同价格、交付时间等原因未成功达成定制订单的，订金作为律师前期服务费，不予退还； 2. 合同难易度、质保天数、交付时间都是决定合同价格的因素，预估金额为参考价格，具体价格请咨询律师。 3. 律师的工作时间是9:30-20:30，非工作时间您也可以支付订金，律师上线将优先开启与您的会话，请关注系统消息提醒。
     * Warranty : 1、系统默认文书交付时间为3天，质保天数3天。 2、质保天数，是指在律师交付文书后，您可在质保时间内要求律师修改调整文书； 3、如急需合同文书，或延长质保时间，您可以和律师商议； 4、文书的交付时间、质保天数、难易程度等，都会影响文书价格。
     * Types : [{"Name":"婚姻家事","Price":"39起"},{"Name":"房产土地","Price":"39起"},{"Name":"公司工商","Price":"39起"},{"Name":"交通事故","Price":"39起"},{"Name":"合同纠纷","Price":"39起"},{"Name":"债权债务","Price":"39起"},{"Name":"劳动纠纷","Price":"39起"},{"Name":"治安刑事","Price":"39起"},{"Name":"诉讼仲裁","Price":"39起"},{"Name":"其他","Price":"39起"}]
     */

    private int ID;
    private String Title;
    private String Screenshot;
    private String ListView;
    private String OrderView;
    private int OriginalPrice;
    private int Price;
    private int Deduction;
    private int Category;
    private int VisitCount;
    private int SalesVolume;
    private String Content;
    private String PriceNote;
    private String TimeNote;
    private String Warranty;
    private List<TypesBean> Types;

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public String getTitle()
    {
        return Title;
    }

    public void setTitle(String Title)
    {
        this.Title = Title;
    }

    public String getScreenshot()
    {
        return Screenshot;
    }

    public void setScreenshot(String Screenshot)
    {
        this.Screenshot = Screenshot;
    }

    public String getListView()
    {
        return ListView;
    }

    public void setListView(String ListView)
    {
        this.ListView = ListView;
    }

    public String getOrderView()
    {
        return OrderView;
    }

    public void setOrderView(String OrderView)
    {
        this.OrderView = OrderView;
    }

    public int getOriginalPrice()
    {
        return OriginalPrice;
    }

    public void setOriginalPrice(int OriginalPrice)
    {
        this.OriginalPrice = OriginalPrice;
    }

    public int getPrice()
    {
        return Price;
    }

    public void setPrice(int Price)
    {
        this.Price = Price;
    }

    public int getDeduction()
    {
        return Deduction;
    }

    public void setDeduction(int Deduction)
    {
        this.Deduction = Deduction;
    }

    public int getCategory()
    {
        return Category;
    }

    public void setCategory(int Category)
    {
        this.Category = Category;
    }

    public int getVisitCount()
    {
        return VisitCount;
    }

    public void setVisitCount(int VisitCount)
    {
        this.VisitCount = VisitCount;
    }

    public int getSalesVolume()
    {
        return SalesVolume;
    }

    public void setSalesVolume(int SalesVolume)
    {
        this.SalesVolume = SalesVolume;
    }

    public String getContent()
    {
        return Content;
    }

    public void setContent(String Content)
    {
        this.Content = Content;
    }

    public String getPriceNote()
    {
        return PriceNote;
    }

    public void setPriceNote(String PriceNote)
    {
        this.PriceNote = PriceNote;
    }

    public String getTimeNote()
    {
        return TimeNote;
    }

    public void setTimeNote(String TimeNote)
    {
        this.TimeNote = TimeNote;
    }

    public String getWarranty()
    {
        return Warranty;
    }

    public void setWarranty(String Warranty)
    {
        this.Warranty = Warranty;
    }

    public List<TypesBean> getTypes()
    {
        return Types;
    }

    public void setTypes(List<TypesBean> Types)
    {
        this.Types = Types;
    }

    public static class TypesBean implements IPickerViewData
    {
        /**
         * Name : 婚姻家事
         * Price : 39起
         */

        private String Name;
        private String Price;

        public String getName()
        {
            return Name;
        }

        public void setName(String Name)
        {
            this.Name = Name;
        }

        public String getPrice()
        {
            return Price;
        }

        public void setPrice(String Price)
        {
            this.Price = Price;
        }

        @Override
        public String getPickerViewText()
        {
            return Name;
        }
    }
}
