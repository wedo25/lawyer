package com.wei.lawyer.model;

/**
 * 作者：赵若位
 * 时间：2018/10/30 20:24
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class LawyerCommentBean
{

    /**
     * ID : 239890
     * Avartar : http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJuPPo6bJsib4E2jHK60X9flmDsFM8jscLoWia0icQnZu5rZAcXARroMzNqcm4c9IZU17Fpgo7OdAqcQ/132
     * Nickname : Moonlight
     * NickName : Moonlight
     * Content : 非常耐心，讲解详细。万分感谢！
     */

    private int ID;
    private String Avartar;
    private String Nickname;
    private String NickName;
    private String Content;

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public String getAvartar()
    {
        return Avartar;
    }

    public void setAvartar(String Avartar)
    {
        this.Avartar = Avartar;
    }

    public String getNickname()
    {
        return Nickname;
    }

    public void setNickname(String Nickname)
    {
        this.Nickname = Nickname;
    }

    public String getNickName()
    {
        return NickName;
    }

    public void setNickName(String NickName)
    {
        this.NickName = NickName;
    }

    public String getContent()
    {
        return Content;
    }

    public void setContent(String Content)
    {
        this.Content = Content;
    }
}
