package com.wei.lawyer.model;

import com.contrarywind.interfaces.IPickerViewData;

/**
 * 作者：赵若位
 * 时间：2018/11/12 11:49
 * 邮箱：1070138445@qq.com
 * 功能：律师擅长的技能
 */
public class FieldBean implements IPickerViewData
{

    /**
     * ID : 1
     * Name : 婚姻家事
     * Image : https://oduep5a2h.qnssl.com/pics/consult/marriage.png
     */

    private int ID;
    private String Name;
    private String Image;

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public String getName()
    {
        return Name;
    }

    public void setName(String Name)
    {
        this.Name = Name;
    }

    public String getImage()
    {
        return Image;
    }

    public void setImage(String Image)
    {
        this.Image = Image;
    }

    @Override
    public String getPickerViewText()
    {
        return Name;
    }
}
