package com.wei.lawyer.model;

/**
 * 作者：赵若位
 * 时间：2018/12/10 16:28
 * 邮箱：1070138445@qq.com
 * 功能：App功能配置
 */
public class ConfigBean
{

    /**
     * QueueCancelSeconds : 5
     * WorkStartTime : 1230
     * WorkEndTime : 570
     * WeekendWorkStartTime : 570
     * WeekendWorkEndTime : 570
     * OutOfWorkTimeTip : 法律顾问的工时时间是9:30-22:00
     * OutOfWeekendWorkTimeTip : 法律顾问的工时时间是9:30-22:00
     * InvitationPoint : 5
     * WarrantTip : 1.质保时间自首次交付定制文件时起算，您可在该时间段内申请质保；
     * 2.质保申请成功提交后，法律顾问会在24小时内交付修改版；
     * 3.定制文件修改时间包含在质保时间内，修改期间您不得再次申请质保；
     * 4.交付修改版后仍在质保时间内的，您可再次申请质保。
     * RefundTip : 1、您可在7个工作日内申请退款，申请提交成功，工作人员将在1个工作日内审核您的订单；
     * 2、请保持电话畅通并及时关注系统消息通知，工作人员将会通过电话或系统消息通知方式与您联系；
     * 3、经工作人员审核，满足退款条件的订单，款项将退还至您的账户余额，可至APP【我】的页面查看。
     * CusPriceParam : [{"Name":"交付时间","Choices":[{"Name":"2天","Value":0.4},{"Name":"3天","Value":0.2},{"Name":"5天","Value":0.1}]},{"Name":"质保天数","Choices":[{"Name":"3天","Value":0.1},{"Name":"7天","Value":0.2},{"Name":"10天","Value":0.4}]}]
     * OrderDepositConsHours : 48
     * OrderTimeoutHours : 48
     * CusServiceStartTime : 570
     * CusServiceEndTime : 1110
     * CusServiceOutOfTimeTip : 客服工作时间：周一至周五9:30~18:00，节假日休息
     * ConsMinuteCost : 1
     * ConsOriginMinuteCost : 3
     * InterlocutionRefundTip : 1、律师未回复、服务态度差、回答存在明显错误等，您均可在7个工作日内申请退款，工作人员将在1个工作日内审核您的订单；
     * 2、请保持电话畅通并及时关注系统消息通知，工作人员将会通过电话或系统消息通知方式与您联系；
     * 3、经工作人员审核，满足退款条件的订单，款项将退还至您的账户余额，可至APP【我】的页面查看。
     * MinLawyerPrice : 1600
     * InterlocutionPayTip :
     * InterlocutionChatTip : 1、本次咨询1小时内可与律师无限对话；
     * 2、律师15分钟内未回复，可结束咨询，至【我的咨询】- 【历史】咨询处申请退款；
     * 3、如需退款请在订单购买之日起7个工作日内提交申请，符合退款条件，款项将返还至您的账户余额；
     * 4、若急需律师回复，可至首页选择【快速咨询】咨询律师。
     * OrderDepositPrice : 3000
     * DelegationDepositPrice : 3000
     * UrgentChatPrice : 800
     * CallBroadcastPrice : 3000
     * CallBroadcastMinutes : 15
     * CallDirectMinutes : 30
     */

    private int QueueCancelSeconds;
    private int WorkStartTime;
    private int WorkEndTime;
    private int WeekendWorkStartTime;
    private int WeekendWorkEndTime;
    private String OutOfWorkTimeTip;
    private String OutOfWeekendWorkTimeTip;
    private int InvitationPoint;
    private String WarrantTip;
    private String RefundTip;
    private String CusPriceParam;
    private int OrderDepositConsHours;
    private int OrderTimeoutHours;
    private int CusServiceStartTime;
    private int CusServiceEndTime;
    private String CusServiceOutOfTimeTip;
    private int ConsMinuteCost;
    private int ConsOriginMinuteCost;
    private String InterlocutionRefundTip;
    private int MinLawyerPrice;
    private String InterlocutionPayTip;
    private String InterlocutionChatTip;
    private int OrderDepositPrice;
    private int DelegationDepositPrice;
    private int UrgentChatPrice;
    private int CallBroadcastPrice;
    private int CallBroadcastMinutes;
    private int CallDirectMinutes;

    public int getQueueCancelSeconds()
    {
        return QueueCancelSeconds;
    }

    public void setQueueCancelSeconds(int QueueCancelSeconds)
    {
        this.QueueCancelSeconds = QueueCancelSeconds;
    }

    public int getWorkStartTime()
    {
        return WorkStartTime;
    }

    public void setWorkStartTime(int WorkStartTime)
    {
        this.WorkStartTime = WorkStartTime;
    }

    public int getWorkEndTime()
    {
        return WorkEndTime;
    }

    public void setWorkEndTime(int WorkEndTime)
    {
        this.WorkEndTime = WorkEndTime;
    }

    public int getWeekendWorkStartTime()
    {
        return WeekendWorkStartTime;
    }

    public void setWeekendWorkStartTime(int WeekendWorkStartTime)
    {
        this.WeekendWorkStartTime = WeekendWorkStartTime;
    }

    public int getWeekendWorkEndTime()
    {
        return WeekendWorkEndTime;
    }

    public void setWeekendWorkEndTime(int WeekendWorkEndTime)
    {
        this.WeekendWorkEndTime = WeekendWorkEndTime;
    }

    public String getOutOfWorkTimeTip()
    {
        return OutOfWorkTimeTip;
    }

    public void setOutOfWorkTimeTip(String OutOfWorkTimeTip)
    {
        this.OutOfWorkTimeTip = OutOfWorkTimeTip;
    }

    public String getOutOfWeekendWorkTimeTip()
    {
        return OutOfWeekendWorkTimeTip;
    }

    public void setOutOfWeekendWorkTimeTip(String OutOfWeekendWorkTimeTip)
    {
        this.OutOfWeekendWorkTimeTip = OutOfWeekendWorkTimeTip;
    }

    public int getInvitationPoint()
    {
        return InvitationPoint;
    }

    public void setInvitationPoint(int InvitationPoint)
    {
        this.InvitationPoint = InvitationPoint;
    }

    public String getWarrantTip()
    {
        return WarrantTip;
    }

    public void setWarrantTip(String WarrantTip)
    {
        this.WarrantTip = WarrantTip;
    }

    public String getRefundTip()
    {
        return RefundTip;
    }

    public void setRefundTip(String RefundTip)
    {
        this.RefundTip = RefundTip;
    }

    public String getCusPriceParam()
    {
        return CusPriceParam;
    }

    public void setCusPriceParam(String CusPriceParam)
    {
        this.CusPriceParam = CusPriceParam;
    }

    public int getOrderDepositConsHours()
    {
        return OrderDepositConsHours;
    }

    public void setOrderDepositConsHours(int OrderDepositConsHours)
    {
        this.OrderDepositConsHours = OrderDepositConsHours;
    }

    public int getOrderTimeoutHours()
    {
        return OrderTimeoutHours;
    }

    public void setOrderTimeoutHours(int OrderTimeoutHours)
    {
        this.OrderTimeoutHours = OrderTimeoutHours;
    }

    public int getCusServiceStartTime()
    {
        return CusServiceStartTime;
    }

    public void setCusServiceStartTime(int CusServiceStartTime)
    {
        this.CusServiceStartTime = CusServiceStartTime;
    }

    public int getCusServiceEndTime()
    {
        return CusServiceEndTime;
    }

    public void setCusServiceEndTime(int CusServiceEndTime)
    {
        this.CusServiceEndTime = CusServiceEndTime;
    }

    public String getCusServiceOutOfTimeTip()
    {
        return CusServiceOutOfTimeTip;
    }

    public void setCusServiceOutOfTimeTip(String CusServiceOutOfTimeTip)
    {
        this.CusServiceOutOfTimeTip = CusServiceOutOfTimeTip;
    }

    public int getConsMinuteCost()
    {
        return ConsMinuteCost;
    }

    public void setConsMinuteCost(int ConsMinuteCost)
    {
        this.ConsMinuteCost = ConsMinuteCost;
    }

    public int getConsOriginMinuteCost()
    {
        return ConsOriginMinuteCost;
    }

    public void setConsOriginMinuteCost(int ConsOriginMinuteCost)
    {
        this.ConsOriginMinuteCost = ConsOriginMinuteCost;
    }

    public String getInterlocutionRefundTip()
    {
        return InterlocutionRefundTip;
    }

    public void setInterlocutionRefundTip(String InterlocutionRefundTip)
    {
        this.InterlocutionRefundTip = InterlocutionRefundTip;
    }

    public int getMinLawyerPrice()
    {
        return MinLawyerPrice;
    }

    public void setMinLawyerPrice(int MinLawyerPrice)
    {
        this.MinLawyerPrice = MinLawyerPrice;
    }

    public String getInterlocutionPayTip()
    {
        return InterlocutionPayTip;
    }

    public void setInterlocutionPayTip(String InterlocutionPayTip)
    {
        this.InterlocutionPayTip = InterlocutionPayTip;
    }

    public String getInterlocutionChatTip()
    {
        return InterlocutionChatTip;
    }

    public void setInterlocutionChatTip(String InterlocutionChatTip)
    {
        this.InterlocutionChatTip = InterlocutionChatTip;
    }

    public int getOrderDepositPrice()
    {
        return OrderDepositPrice;
    }

    public void setOrderDepositPrice(int OrderDepositPrice)
    {
        this.OrderDepositPrice = OrderDepositPrice;
    }

    public int getDelegationDepositPrice()
    {
        return DelegationDepositPrice;
    }

    public void setDelegationDepositPrice(int DelegationDepositPrice)
    {
        this.DelegationDepositPrice = DelegationDepositPrice;
    }

    public int getUrgentChatPrice()
    {
        return UrgentChatPrice;
    }

    public void setUrgentChatPrice(int UrgentChatPrice)
    {
        this.UrgentChatPrice = UrgentChatPrice;
    }

    public int getCallBroadcastPrice()
    {
        return CallBroadcastPrice;
    }

    public void setCallBroadcastPrice(int CallBroadcastPrice)
    {
        this.CallBroadcastPrice = CallBroadcastPrice;
    }

    public int getCallBroadcastMinutes()
    {
        return CallBroadcastMinutes;
    }

    public void setCallBroadcastMinutes(int CallBroadcastMinutes)
    {
        this.CallBroadcastMinutes = CallBroadcastMinutes;
    }

    public int getCallDirectMinutes()
    {
        return CallDirectMinutes;
    }

    public void setCallDirectMinutes(int CallDirectMinutes)
    {
        this.CallDirectMinutes = CallDirectMinutes;
    }
}
