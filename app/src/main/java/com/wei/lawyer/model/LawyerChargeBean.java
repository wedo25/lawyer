package com.wei.lawyer.model;

import com.wei.lawyer.App;
import com.wei.lawyer.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/10/30 20:08
 * 邮箱：1070138445@qq.com
 * 功能：律师详情收费实体类
 */
public class LawyerChargeBean
{
    /*收费种类图标*/
    private int iconResource;
    /*服务名称*/
    private String title;
    /*服务描述*/
    private String descript;
    /*服务价格*/
    private int price;


    /*律师详情页面收费标准*/
    public static List<LawyerChargeBean> getList(LawyerBean data)
    {
        List<LawyerChargeBean> list = new ArrayList<>();
        list.add(new LawyerChargeBean(R.mipmap.lawyer_tuwen, "图文咨询", "1小时内无限对话", data != null ? data.getConsPrice() / 100 : 0));
        list.add(new LawyerChargeBean(R.mipmap.lawyer_phone, "电话专问", "高效沟通方便快捷", data != null ? data.getCallPrice() / 100 : 0));
        list.add(new LawyerChargeBean(R.mipmap.lawyer_deposit, "文书服务定金", "文书定制、合同审查/修改",
                App.mConfig != null ? App.mConfig.getOrderDepositPrice() / 100 : 0));
        list.add(new LawyerChargeBean(R.mipmap.lawyer_case, "案件委托定金", "打官司·约见律师",
                App.mConfig != null ? App.mConfig.getDelegationDepositPrice() / 100 : 0));
        return list;
    }


    public LawyerChargeBean(int iconResource, String title, String descript, int price)
    {
        this.iconResource = iconResource;
        this.title = title;
        this.descript = descript;
        this.price = price;
    }

    public int getIconResource()
    {
        return iconResource;
    }

    public void setIconResource(int iconResource)
    {
        this.iconResource = iconResource;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescript()
    {
        return descript;
    }

    public void setDescript(String descript)
    {
        this.descript = descript;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }
}
