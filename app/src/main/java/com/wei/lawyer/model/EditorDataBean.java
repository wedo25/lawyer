package com.wei.lawyer.model;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.IntDef;

import com.wei.lawyer.App;
import com.wei.lawyer.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 作者：赵若位
 * 时间：2018/11/1 13:15
 * 邮箱：1070138445@qq.com
 * 功能：编辑页面传输对象
 */
public class EditorDataBean implements Parcelable
{
    /*编辑页面标题*/
    private String mTitle;
    /*编辑页面EditText提示话语*/
    private String mHint;
    /*编辑的类型*/
    private @Type
    int mEditorType;
    /*初始化输入内容*/
    private String mDescript;


    /*修改昵称*/
    public static final int MODIFYNICK = 1;
    /*修改个人简介*/
    public static final int MODIFYDESCRIPT = 2;

    public EditorDataBean()
    {
    }

    public EditorDataBean(@Type int type, String msg)
    {
        Resources resources = App.getContext().getResources();
        this.mEditorType = type;
        this.mDescript = msg;
        switch (type)
        {
            case MODIFYNICK:
                this.mTitle = resources.getString(R.string.tv_setting_editor_nick);
                this.mHint = resources.getString(R.string.tv_setting_editor_nick_hint);
                break;
            case MODIFYDESCRIPT:
                this.mTitle = resources.getString(R.string.tv_setting_editor_descript);
                this.mHint = resources.getString(R.string.tv_setting_editor_descript_hint);
                break;
            default:
                break;
        }
    }


    protected EditorDataBean(Parcel in)
    {
        mTitle = in.readString();
        mHint = in.readString();
        mEditorType = in.readInt();
        mDescript = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(mTitle);
        dest.writeString(mHint);
        dest.writeInt(mEditorType);
        dest.writeString(mDescript);
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    public static final Creator<EditorDataBean> CREATOR = new Creator<EditorDataBean>()
    {
        @Override
        public EditorDataBean createFromParcel(Parcel in)
        {
            return new EditorDataBean(in);
        }

        @Override
        public EditorDataBean[] newArray(int size)
        {
            return new EditorDataBean[size];
        }
    };

    public String getTitle()
    {
        return mTitle;
    }

    public void setTitle(String title)
    {
        mTitle = title;
    }

    public String getHint()
    {
        return mHint;
    }

    public void setHint(String hint)
    {
        mHint = hint;
    }

    @Type
    public int getEditorType()
    {
        return mEditorType;
    }

    public void setEditorType(@Type int type)
    {
        mEditorType = type;
    }

    public String getDescript()
    {
        return mDescript;
    }

    public void setDescript(String descript)
    {
        mDescript = descript;
    }

    @IntDef({MODIFYNICK, MODIFYDESCRIPT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type
    {

    }
}
