package com.wei.lawyer.model;

/**
 * 作者：赵若位
 * 时间：2018/11/6 20:20
 * 邮箱：1070138445@qq.com
 * 功能：法条搜索对象
 */
public class LawSearchBean
{
    /*标题*/
    private String mTitle;
    /*描述*/
    private String mDescript;
    /*作者*/
    private String mAuthor;
    /*类型*/
    private String mType;

    public LawSearchBean()
    {
    }


    public LawSearchBean(String msg)
    {
        mTitle = msg + ":如果你去问从事金融投行、医药研发、移动互联网的人，而且已经在他们的行业当上了高管以上的职位，那他们肯定会跟你说，月入1万肯定大把，或者干脆说，他们会认为，在中国还拿着低于1万块钱的工资，简直是一种耻辱。如果把视线移到中国广大的农村，尤其是中西部比较偏远的山区，月入1万块钱对于他们来说，可以说是非常高的了，在他们的眼里每个月都能稳定拿到这样高的工资，已经是非常幸福的了。也就是说，月入1万块钱的人多不多，取决于你询问的对象。";
        mDescript = "如果你去问从事金融投行、医药研发、移动互联网的人，而且已经在他们的行业当上了高管以上的职位，那他们肯定会跟你说，月入1万肯定大把，或者干脆说，他们会认为，在中国还拿着低于1万块钱的工资，简直是一种耻辱。如果把视线移到中国广大的农村，尤其是中西部比较偏远的山区，月入1万块钱对于他们来说，可以说是非常高的了，在他们的眼里每个月都能稳定拿到这样高的工资，已经是非常幸福的了。也就是说，月入1万块钱的人多不多，取决于你询问的对象。";
        mAuthor = "最高人民法院";
        mType = "现行有效";
    }

    public String getTitle()
    {
        return mTitle;
    }

    public void setTitle(String title)
    {
        mTitle = title;
    }

    public String getDescript()
    {
        return mDescript;
    }

    public void setDescript(String descript)
    {
        mDescript = descript;
    }

    public String getAuthor()
    {
        return mAuthor;
    }

    public void setAuthor(String author)
    {
        mAuthor = author;
    }

    public String getType()
    {
        return mType;
    }

    public void setType(String type)
    {
        mType = type;
    }
}
