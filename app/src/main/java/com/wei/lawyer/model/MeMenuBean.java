package com.wei.lawyer.model;

import android.content.Context;
import android.util.Log;

import com.wei.lawyer.R;
import com.wei.lawyer.utils.LogUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/10/31 21:12
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class MeMenuBean
{
    private int imageRescource;
    private String title;
    private String descript;

    public static List<MeMenuBean> getList()
    {
        List<MeMenuBean> list = new ArrayList<>();
        list.add(new MeMenuBean(R.mipmap.self_consult,"我的咨询",""));
        list.add(new MeMenuBean(R.mipmap.self_order,"我的订单",""));
        list.add(new MeMenuBean(R.mipmap.self_collect,"我的收藏","合同|法条"));
        list.add(new MeMenuBean(R.mipmap.self_law,"法条搜索",""));

        list.add(new MeMenuBean(R.mipmap.self_verify,"律师认证",""));
        list.add(new MeMenuBean(R.mipmap.self_invite,"邀请好友","+5律币"));
        list.add(new MeMenuBean(R.mipmap.self_help,"投诉建议",""));
        list.add(new MeMenuBean(R.mipmap.self_set, "设置", ""));
        return list;
    }




    public MeMenuBean(int imageRescource, String title, String descript)
    {
        this.imageRescource = imageRescource;
        this.title = title;
        this.descript = descript;
    }


    public int getImageRescource()
    {
        return imageRescource;
    }

    public void setImageRescource(int imageRescource)
    {
        this.imageRescource = imageRescource;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescript()
    {
        return descript;
    }

    public void setDescript(String descript)
    {
        this.descript = descript;
    }
}
