package com.wei.lawyer.model;

import androidx.annotation.IntegerRes;

import com.wei.lawyer.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/12/13 11:03
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class GiftBean
{

    private @IntegerRes
    int resource;
    private int price;


    public GiftBean(int resource, int price)
    {
        this.resource = resource;
        this.price = price;
    }

    public int getResource()
    {
        return resource;
    }

    public void setResource(int resource)
    {
        this.resource = resource;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }


    public static List<GiftBean> getGiftList()
    {
        List<GiftBean> list = new ArrayList<>();
        list.add(new GiftBean(R.mipmap.reward_5, 5));
        list.add(new GiftBean(R.mipmap.reward_10, 10));
        list.add(new GiftBean(R.mipmap.reward_20, 20));
        list.add(new GiftBean(R.mipmap.reward_30, 30));
        list.add(new GiftBean(R.mipmap.reward_50, 50));
        list.add(new GiftBean(R.mipmap.reward_66, 66));
        list.add(new GiftBean(R.mipmap.reward_88, 88));
        list.add(new GiftBean(R.mipmap.reward_128, 128));
        list.add(new GiftBean(R.mipmap.reward_188, 188));
        return list;
    }

}
