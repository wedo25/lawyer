package com.wei.lawyer.model;

import android.content.Context;

import com.wei.lawyer.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/11/8 18:43
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class ContractSearchBean
{
    private String mTitle;
    private List<ContractBean> mList;


    public ContractSearchBean()
    {
    }


    public ContractSearchBean(String title, List<ContractBean> list)
    {
        mTitle = title;
        mList = list;
    }


    public String getTitle()
    {
        return mTitle;
    }

    public void setTitle(String title)
    {
        mTitle = title;
    }

    public List<ContractBean> getList()
    {
        return mList;
    }

    public void setList(List<ContractBean> list)
    {
        mList = list;
    }
}
