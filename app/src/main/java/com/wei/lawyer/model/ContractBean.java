package com.wei.lawyer.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.wei.lawyer.App;
import com.wei.lawyer.sql.ContractBeanDao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;

import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/11/7 16:11
 * 邮箱：1070138445@qq.com
 * 功能：合同模板
 */
@Entity
public class ContractBean implements Parcelable
{
    /**
     * ID : 201
     * Title : 借款合同（股权质押担保）
     * Description : 【因合同模板的特殊性，一旦购买成功，视为已享受服务，不予退款。】如遇合同下载、打开等问题，请至“投诉建议”处联系平台客服。 乙方以股权作为质押担保，向甲方借款。
     * Screenshot : https://assets.cdn.lvlvlaw.com/contract_covers/4be87c82-9d55-a7f7-2114-bcec1f47d7cf.jpg
     * ListView : https://assets.cdn.lvlvlaw.com/contract_covers/4be87c82-9d55-a7f7-2114-bcec1f47d7cf.jpg
     * OrderView : https://archives.cdn.lvlvlaw.com/b332bffb-cbe8-a7c5-8b41-93d9d35ffeac.png
     * OriginalPrice : 14900
     * Price : 4990
     * Deduction : 10
     * Category : 2
     * VisitCount : 3202
     * SalesVolume : 2
     * Related : [{"ID":29,"Title":"借款合同（个人与个人）"},{"ID":535,"Title":"借条（个人与个人之间借款）"},{"ID":536,"Title":"欠条"}]
     * Screenshots : ["https://assets.cdn.lvlvlaw.com/contract_covers/4be87c82-9d55-a7f7-2114-bcec1f47d7cf.jpg","https://assets.cdn.lvlvlaw.com/contract_screenshots/cd99ee63-7c64-2d30-f2be-284a9563f8c6.jpg","https://assets.cdn.lvlvlaw.com/contract_screenshots/fdd59bce-d5fc-7874-34d6-9c0269bcbfee.jpg"]
     */

    private int ID;
    @Id
    private String Title;
    private String Description;
    private String Screenshot;
    private String ListView;
    private String OrderView;
    private int OriginalPrice;
    private int Price;
    private int Deduction;
    private int Category;
    private int VisitCount;
    private int SalesVolume;
    @Transient
    private List<RelatedBean> Related;
    @Transient
    private List<String> Screenshots;
    /*是否被收藏*/
    private boolean collection;
    /*本地存储的时间*/
    private long createTime;


    public ContractBean()
    {
    }


    protected ContractBean(Parcel in)
    {
        ID = in.readInt();
        Title = in.readString();
        Description = in.readString();
        Screenshot = in.readString();
        ListView = in.readString();
        OrderView = in.readString();
        OriginalPrice = in.readInt();
        Price = in.readInt();
        Deduction = in.readInt();
        Category = in.readInt();
        VisitCount = in.readInt();
        SalesVolume = in.readInt();
        Screenshots = in.createStringArrayList();
        collection = in.readByte() != 0;
    }


    @Generated(hash = 1801989081)
    public ContractBean(int ID, String Title, String Description, String Screenshot, String ListView, String OrderView, int OriginalPrice, int Price, int Deduction, int Category, int VisitCount, int SalesVolume, boolean collection, long createTime)
    {
        this.ID = ID;
        this.Title = Title;
        this.Description = Description;
        this.Screenshot = Screenshot;
        this.ListView = ListView;
        this.OrderView = OrderView;
        this.OriginalPrice = OriginalPrice;
        this.Price = Price;
        this.Deduction = Deduction;
        this.Category = Category;
        this.VisitCount = VisitCount;
        this.SalesVolume = SalesVolume;
        this.collection = collection;
        this.createTime = createTime;
    }


    public static final Creator<ContractBean> CREATOR = new Creator<ContractBean>()
    {
        @Override
        public ContractBean createFromParcel(Parcel in)
        {
            return new ContractBean(in);
        }

        @Override
        public ContractBean[] newArray(int size)
        {
            return new ContractBean[size];
        }
    };

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public String getTitle()
    {
        return Title;
    }

    public void setTitle(String Title)
    {
        this.Title = Title;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String Description)
    {
        this.Description = Description;
    }

    public String getScreenshot()
    {
        return Screenshot;
    }

    public void setScreenshot(String Screenshot)
    {
        this.Screenshot = Screenshot;
    }

    public String getListView()
    {
        return ListView;
    }

    public void setListView(String ListView)
    {
        this.ListView = ListView;
    }

    public String getOrderView()
    {
        return OrderView;
    }

    public void setOrderView(String OrderView)
    {
        this.OrderView = OrderView;
    }

    public int getOriginalPrice()
    {
        return OriginalPrice;
    }

    public void setOriginalPrice(int OriginalPrice)
    {
        this.OriginalPrice = OriginalPrice;
    }

    public int getPrice()
    {
        return Price;
    }

    public void setPrice(int Price)
    {
        this.Price = Price;
    }

    public int getDeduction()
    {
        return Deduction;
    }

    public void setDeduction(int Deduction)
    {
        this.Deduction = Deduction;
    }

    public int getCategory()
    {
        return Category;
    }

    public void setCategory(int Category)
    {
        this.Category = Category;
    }

    public int getVisitCount()
    {
        return VisitCount;
    }

    public void setVisitCount(int VisitCount)
    {
        this.VisitCount = VisitCount;
    }

    public int getSalesVolume()
    {
        return SalesVolume;
    }

    public void setSalesVolume(int SalesVolume)
    {
        this.SalesVolume = SalesVolume;
    }

    public List<RelatedBean> getRelated()
    {
        return Related;
    }

    public void setRelated(List<RelatedBean> Related)
    {
        this.Related = Related;
    }

    public List<String> getScreenshots()
    {
        return Screenshots;
    }

    public void setScreenshots(List<String> Screenshots)
    {
        this.Screenshots = Screenshots;
    }


    public boolean isCollection()
    {
        return collection;
    }

    public void setCollection(boolean collection)
    {
        this.collection = collection;
    }


    public boolean getCollection()
    {
        return this.collection;
    }


    public long getCreateTime()
    {
        return this.createTime;
    }


    public void setCreateTime(long createTime)
    {
        this.createTime = createTime;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(ID);
        dest.writeString(Title);
        dest.writeString(Description);
        dest.writeString(Screenshot);
        dest.writeString(ListView);
        dest.writeString(OrderView);
        dest.writeInt(OriginalPrice);
        dest.writeInt(Price);
        dest.writeInt(Deduction);
        dest.writeInt(Category);
        dest.writeInt(VisitCount);
        dest.writeInt(SalesVolume);
        dest.writeStringList(Screenshots);
        dest.writeByte((byte) (collection ? 1 : 0));
        dest.writeLong(createTime);
    }

    public static class RelatedBean
    {
        /**
         * ID : 29
         * Title : 借款合同（个人与个人）
         */

        private int ID;
        private String Title;

        public int getID()
        {
            return ID;
        }

        public void setID(int ID)
        {
            this.ID = ID;
        }

        public String getTitle()
        {
            return Title;
        }

        public void setTitle(String Title)
        {
            this.Title = Title;
        }
    }


    /*将搜索数据插入到数据库*/
    public void insertOrReplace()
    {
        if (App.mSession != null)
        {
            ContractBeanDao dao = App.mSession.getContractBeanDao();
            this.createTime = System.currentTimeMillis();
            dao.insertOrReplaceInTx(this);
        }
    }
}
