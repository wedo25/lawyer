package com.wei.lawyer.model;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;

import com.wei.lawyer.App;
import com.wei.lawyer.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/11/15 12:20
 * 邮箱：1070138445@qq.com
 * 功能：企业服务实体对象
 */
public class CompanyServiceBean implements Parcelable
{
    /**
     * ID : 18
     * Title : 8份合同起草及修改
     * Description : 合同起草、修改
     * Screenshot : https://og0f478en.qnssl.com/1db666a6-647f-4c9c-8722-41d1309ea303.png
     * ListView : https://assets.cdn.lvlvlaw.com/services_screenshot/fe57f2e7-70b0-d099-64c9-cc33832de501.png
     * OrderView : https://archives.cdn.lvlvlaw.com/dbc93d6b-dfa2-ea35-77e2-7bc7314b431a.png
     * OriginalPrice : 399800
     * Price : 5000
     * Deduction : 0
     * Category : 400
     * VisitCount : 272342
     * SalesVolume : 46
     * Content : 1. 在服务有效期期内，用户可享受8份合同的起草修订及法律风险的书面提示服务，由专业律师审查、指导，保障用户的合法权益。 2. 合同类型限于公司工商类、买卖交易类、股权融资类、债权债务类、物权担保类、劳动类、房产土地类、租赁类、婚姻继承类、赡养抚养类、交通类。
     * PriceNote : 1.该定价所包涵的合同仅限于中文，不含涉外合同（涉及港澳台地区，参照涉外合同处理） 2.订金可抵扣服务费，若订单未达成，订金作为律师前期服务费，不予退还（律师有权不回答与该笔订金业务无关的法律问题）；
     * TimeNote : 律师在收到客户按照约定的方式发出的服务申请后48小时内给予反馈。
     */

    private int ID;
    private String Title;
    private String Description;
    private String Screenshot;
    private String ListView;
    private String OrderView;
    private int OriginalPrice;
    private int Price;
    private int Deduction;
    private int Category;
    private int VisitCount;
    private int SalesVolume;
    private String Content;
    private String PriceNote;
    private String TimeNote;
    /*详情页标题*/
    private String Service;
    /*评论*/
    private List<LawyerCommentBean> Comments;
    /*服务流程*/
    private List<ServiceData> mServiceData;


    public CompanyServiceBean()
    {
    }

    protected CompanyServiceBean(Parcel in)
    {
        ID = in.readInt();
        Title = in.readString();
        Description = in.readString();
        Screenshot = in.readString();
        ListView = in.readString();
        OrderView = in.readString();
        OriginalPrice = in.readInt();
        Price = in.readInt();
        Deduction = in.readInt();
        Category = in.readInt();
        VisitCount = in.readInt();
        SalesVolume = in.readInt();
        Content = in.readString();
        PriceNote = in.readString();
        TimeNote = in.readString();
        Service = in.readString();
    }

    public static final Creator<CompanyServiceBean> CREATOR = new Creator<CompanyServiceBean>()
    {
        @Override
        public CompanyServiceBean createFromParcel(Parcel in)
        {
            return new CompanyServiceBean(in);
        }

        @Override
        public CompanyServiceBean[] newArray(int size)
        {
            return new CompanyServiceBean[size];
        }
    };

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public String getTitle()
    {
        return Title;
    }

    public void setTitle(String Title)
    {
        this.Title = Title;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String Description)
    {
        this.Description = Description;
    }

    public String getScreenshot()
    {
        return Screenshot;
    }

    public void setScreenshot(String Screenshot)
    {
        this.Screenshot = Screenshot;
    }

    public String getListView()
    {
        return ListView;
    }

    public void setListView(String ListView)
    {
        this.ListView = ListView;
    }

    public String getOrderView()
    {
        return OrderView;
    }

    public void setOrderView(String OrderView)
    {
        this.OrderView = OrderView;
    }

    public int getOriginalPrice()
    {
        return OriginalPrice;
    }

    public void setOriginalPrice(int OriginalPrice)
    {
        this.OriginalPrice = OriginalPrice;
    }

    public int getPrice()
    {
        return Price;
    }

    public void setPrice(int Price)
    {
        this.Price = Price;
    }

    public int getDeduction()
    {
        return Deduction;
    }

    public void setDeduction(int Deduction)
    {
        this.Deduction = Deduction;
    }

    public int getCategory()
    {
        return Category;
    }

    public void setCategory(int Category)
    {
        this.Category = Category;
    }

    public int getVisitCount()
    {
        return VisitCount;
    }

    public void setVisitCount(int VisitCount)
    {
        this.VisitCount = VisitCount;
    }

    public int getSalesVolume()
    {
        return SalesVolume;
    }

    public void setSalesVolume(int SalesVolume)
    {
        this.SalesVolume = SalesVolume;
    }

    public String getContent()
    {
        return Content;
    }

    public void setContent(String Content)
    {
        this.Content = Content;
    }

    public String getPriceNote()
    {
        return PriceNote;
    }

    public void setPriceNote(String PriceNote)
    {
        this.PriceNote = PriceNote;
    }

    public String getTimeNote()
    {
        return TimeNote;
    }

    public void setTimeNote(String TimeNote)
    {
        this.TimeNote = TimeNote;
    }


    public String getService()
    {
        return Service;
    }

    public void setService(String service)
    {
        Service = service;
    }


    public List<LawyerCommentBean> getComments()
    {
        return Comments;
    }

    public void setComments(List<LawyerCommentBean> comments)
    {
        Comments = comments;
    }


    public List<ServiceData> getServiceData()
    {
        return mServiceData;
    }

    public void setServiceData()
    {
        Resources resources = App.getContext().getResources();
        if (mServiceData == null)
        {
            mServiceData = new ArrayList<>();
        }
        mServiceData.add(new ServiceData(resources.getString(R.string.tv_service_process), ""));
        mServiceData.add(new ServiceData(resources.getString(R.string.tv_service_content), this.Content));
        mServiceData.add(new ServiceData(resources.getString(R.string.tv_price_descript), this.PriceNote));
        mServiceData.add(new ServiceData(resources.getString(R.string.tv_service_time), this.TimeNote));
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(ID);
        dest.writeString(Title);
        dest.writeString(Description);
        dest.writeString(Screenshot);
        dest.writeString(ListView);
        dest.writeString(OrderView);
        dest.writeInt(OriginalPrice);
        dest.writeInt(Price);
        dest.writeInt(Deduction);
        dest.writeInt(Category);
        dest.writeInt(VisitCount);
        dest.writeInt(SalesVolume);
        dest.writeString(Content);
        dest.writeString(PriceNote);
        dest.writeString(TimeNote);
        dest.writeString(Service);
    }


    /*服务流程*/
    public static class ServiceData
    {
        private String mTitle;
        private String mDescript;


        public ServiceData(String title, String descript)
        {
            mTitle = title;
            mDescript = descript;
        }

        public String getTitle()
        {
            return mTitle;
        }

        public void setTitle(String title)
        {
            mTitle = title;
        }

        public String getDescript()
        {
            return mDescript;
        }

        public void setDescript(String descript)
        {
            mDescript = descript;
        }
    }

}

