package com.wei.lawyer.model;

import com.contrarywind.interfaces.IPickerViewData;

import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/12/6 23:42
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class ProvinceBean implements IPickerViewData
{
    private String name;
    private List<CityBean> city;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<CityBean> getCity()
    {
        return city;
    }

    public void setCity(List<CityBean> city)
    {
        this.city = city;
    }

    @Override
    public String getPickerViewText()
    {
        return name;
    }

    public static class CityBean implements IPickerViewData
    {
        private String name;
        private List<String> area;

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public List<String> getArea()
        {
            return area;
        }

        public void setArea(List<String> area)
        {
            this.area = area;
        }

        @Override
        public String getPickerViewText()
        {
            return name;
        }
    }
}
