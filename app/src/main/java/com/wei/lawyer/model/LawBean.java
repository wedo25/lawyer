package com.wei.lawyer.model;

/**
 * 作者：赵若位
 * 时间：2018/11/6 9:27
 * 邮箱：1070138445@qq.com
 * 功能：法条
 */
public class LawBean
{

    private String mTitle;
    private String mAuthor;
    private String mTime;


    public LawBean()
    {
        mTitle = "每一个view的所有属性, 坐标, 样式, 状态等一切都可以依赖于另一个view, 因此使得parentView和所有childView之间都可以互相联动起来.想象一下, 不仅仅是定位, 所有可以设置在View上面的属性都可以依赖于另一个view的变化而变化, 某一个View可以跟随另一个View一起滚动, 某一个View可以跟随另一个View的状态改变而改变, 性能非常的强大;";
        mAuthor = "中共中央";
        mTime = "2018-11-02";
    }

    public String getTitle()
    {
        return mTitle;
    }

    public void setTitle(String title)
    {
        mTitle = title;
    }

    public String getAuthor()
    {
        return mAuthor;
    }

    public void setAuthor(String author)
    {
        mAuthor = author;
    }

    public String getTime()
    {
        return mTime;
    }

    public void setTime(String time)
    {
        mTime = time;
    }
}
