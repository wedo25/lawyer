package com.wei.lawyer.model;


import android.text.TextUtils;

import com.wei.lawyer.App;
import com.wei.lawyer.R;
import com.wei.lawyer.net.RetrofitManager;
import com.wei.lawyer.ui.fragment.MeFragment;
import com.wei.lawyer.utils.StringUtils;

import org.greenrobot.eventbus.EventBus;

/**
 * 作者：赵若位
 * 时间：2018/11/29 18:00
 * 邮箱：1070138445@qq.com
 * 功能：用户对象
 */
public class UserBean
{
    /**
     * ID : 477816
     * NickName : 豌豆儿
     * RealName :
     * Description :
     * Gender : true
     * Avatar : https://pub.cdn.lvlvlaw.com/FsovpXgNWONZQevWsxYwvoIrDIw5
     * Company :
     * Occupation :
     * Location : 四川 成都市
     * PostCount : 0
     * TagCount : 0
     * FollowingCount : 0
     * FollowerCount : 0
     * CommentCount : 0
     * PostStarCount : 0
     * ArticleCount : 0
     * LStatus : 0
     * Role : 0
     * InviteCode : 1x703xb355
     * CTime : 2018-06-19 23:23:30
     */

    /*用户ID*/
    private int ID;
    /*用户昵称*/
    private String NickName;
    /*用户真实名称*/
    private String RealName;
    /*用户介绍*/
    private String Description;
    private boolean Gender;
    /*用户头像*/
    private String Avatar;
    private String Company;
    private String Occupation;
    /*用户地址*/
    private String Location;
    private int PostCount;
    private int TagCount;
    private int FollowingCount;
    private int FollowerCount;
    private int CommentCount;
    private int PostStarCount;
    private int ArticleCount;
    private int LStatus;
    private int Role;
    private String InviteCode;
    private String CTime;
    /*用户律币余额*/
    private long Point;
    /*用户现金余额*/
    private double Cash;

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public String getNickName()
    {
        return NickName;
    }

    public void setNickName(String NickName)
    {
        this.NickName = NickName;
    }

    public String getRealName()
    {
        return RealName;
    }

    public void setRealName(String RealName)
    {
        this.RealName = RealName;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String Description)
    {
        this.Description = Description;
    }

    public boolean isGender()
    {
        return Gender;
    }

    public void setGender(boolean Gender)
    {
        this.Gender = Gender;
    }

    public String getAvatar()
    {
        return TextUtils.isEmpty(Avatar) ? RetrofitManager.BASE_URL : Avatar;
    }

    public void setAvatar(String Avatar)
    {
        this.Avatar = Avatar;
    }

    public String getCompany()
    {
        return Company;
    }

    public void setCompany(String Company)
    {
        this.Company = Company;
    }

    public String getOccupation()
    {
        return Occupation;
    }

    public void setOccupation(String Occupation)
    {
        this.Occupation = Occupation;
    }

    public String getLocation()
    {
        return Location;
    }

    public void setLocation(String Location)
    {
        this.Location = Location;
    }

    public int getPostCount()
    {
        return PostCount;
    }

    public void setPostCount(int PostCount)
    {
        this.PostCount = PostCount;
    }

    public int getTagCount()
    {
        return TagCount;
    }

    public void setTagCount(int TagCount)
    {
        this.TagCount = TagCount;
    }

    public int getFollowingCount()
    {
        return FollowingCount;
    }

    public void setFollowingCount(int FollowingCount)
    {
        this.FollowingCount = FollowingCount;
    }

    public int getFollowerCount()
    {
        return FollowerCount;
    }

    public void setFollowerCount(int FollowerCount)
    {
        this.FollowerCount = FollowerCount;
    }

    public int getCommentCount()
    {
        return CommentCount;
    }

    public void setCommentCount(int CommentCount)
    {
        this.CommentCount = CommentCount;
    }

    public int getPostStarCount()
    {
        return PostStarCount;
    }

    public void setPostStarCount(int PostStarCount)
    {
        this.PostStarCount = PostStarCount;
    }

    public int getArticleCount()
    {
        return ArticleCount;
    }

    public void setArticleCount(int ArticleCount)
    {
        this.ArticleCount = ArticleCount;
    }

    public int getLStatus()
    {
        return LStatus;
    }

    public void setLStatus(int LStatus)
    {
        this.LStatus = LStatus;
    }

    public int getRole()
    {
        return Role;
    }

    public void setRole(int Role)
    {
        this.Role = Role;
    }

    public String getInviteCode()
    {
        return InviteCode;
    }

    public void setInviteCode(String InviteCode)
    {
        this.InviteCode = InviteCode;
    }

    public String getCTime()
    {
        return CTime;
    }

    public void setCTime(String CTime)
    {
        this.CTime = CTime;
    }

    public long getPoint()
    {
        return Point;
    }


    public String getPointDescript()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(Point)
                .append("\n")
                .append(App.getContext().getString(R.string.tv_make_point));
        return builder.toString();
    }

    public void setPoint(long point)
    {
        this.Point = point;
    }

    public double getCash()
    {
        return Cash;
    }

    public String getCashDescript()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(App.getContext().getString(R.string.tv_money))
                .append(StringUtils.getDouble2(Cash / 100))
                .append("\n")
                .append(App.getContext().getString(R.string.tv_recarge));
        return builder.toString();
    }

    public void setCash(double cash)
    {
        this.Cash = cash;
    }


    /*更新个人中心用户信息*/
    public static void updateUserInfo()
    {
        //登录成功，向服务器请求用户数据
        BaseDataBean data = new BaseDataBean();
        data.setCode(200);
        data.setMsg(MeFragment.class.getSimpleName());
        EventBus.getDefault().post(data);
    }
}
