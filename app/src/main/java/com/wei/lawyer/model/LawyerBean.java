package com.wei.lawyer.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.math.BigDecimal;

/**
 * 作者：赵若位
 * 时间：2018/10/30 11:51
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class LawyerBean implements Parcelable
{
    /**
     * ID : 637
     * Avatar : https://assets.cdn.lvlvlaw.com/lawyercheck_avatar/4154f9da-a46f-d114-3be2-b47d438fad8c.jpg
     * NickName : 陈钟
     * Location : 江苏 苏州市
     * Description :
     * ProfileID : 526309
     * LawOffice : 江苏瑞生律师事务所
     * ConsPrice : 4000
     * CallPrice : 5000
     * LawyerType : 1
     * SatisfyRatio : 0.968
     * Status : 2
     * LastActivation : 2018-11-28T10:49:38Z
     * ShowOffice : true
     * AnswerCount : 0
     * TagTime : 2018-08-01 10:39:08
     * CareerTime : 6年
     * Tag : 合同纠纷、婚姻家事、交通事故
     * InterDesc : 1小时内无限对话
     */

    private int ID;
    private String Avatar;
    private String NickName;
    private String Location;
    private String Description;
    private int ProfileID;
    private String LawOffice;
    private int ConsPrice;
    private int CallPrice;
    private int LawyerType;
    private double SatisfyRatio;
    private int Status;
    private String LastActivation;
    private boolean ShowOffice;
    private int AnswerCount;
    private String TagTime;
    private String CareerTime;
    private String Tag;
    private String InterDesc;


    public LawyerBean()
    {
    }

    protected LawyerBean(Parcel in)
    {
        ID = in.readInt();
        Avatar = in.readString();
        NickName = in.readString();
        Location = in.readString();
        Description = in.readString();
        ProfileID = in.readInt();
        LawOffice = in.readString();
        ConsPrice = in.readInt();
        CallPrice = in.readInt();
        LawyerType = in.readInt();
        SatisfyRatio = in.readDouble();
        Status = in.readInt();
        LastActivation = in.readString();
        ShowOffice = in.readByte() != 0;
        AnswerCount = in.readInt();
        TagTime = in.readString();
        CareerTime = in.readString();
        Tag = in.readString();
        InterDesc = in.readString();
    }

    public static final Creator<LawyerBean> CREATOR = new Creator<LawyerBean>()
    {
        @Override
        public LawyerBean createFromParcel(Parcel in)
        {
            return new LawyerBean(in);
        }

        @Override
        public LawyerBean[] newArray(int size)
        {
            return new LawyerBean[size];
        }
    };

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public String getAvatar()
    {
        return Avatar;
    }

    public void setAvatar(String Avatar)
    {
        this.Avatar = Avatar;
    }

    public String getNickName()
    {
        return NickName;
    }

    public void setNickName(String NickName)
    {
        this.NickName = NickName;
    }

    public String getLocation()
    {
        return Location;
    }

    public void setLocation(String Location)
    {
        this.Location = Location;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String Description)
    {
        this.Description = Description;
    }

    public int getProfileID()
    {
        return ProfileID;
    }

    public void setProfileID(int ProfileID)
    {
        this.ProfileID = ProfileID;
    }

    public String getLawOffice()
    {
        return LawOffice;
    }

    public void setLawOffice(String LawOffice)
    {
        this.LawOffice = LawOffice;
    }

    public int getConsPrice()
    {
        return ConsPrice;
    }

    public void setConsPrice(int ConsPrice)
    {
        this.ConsPrice = ConsPrice;
    }

    public int getCallPrice()
    {
        return CallPrice;
    }

    public void setCallPrice(int CallPrice)
    {
        this.CallPrice = CallPrice;
    }

    public int getLawyerType()
    {
        return LawyerType;
    }

    public void setLawyerType(int LawyerType)
    {
        this.LawyerType = LawyerType;
    }

    /*获取好评率*/
    public double getSatisfyRatio()
    {
        return SatisfyRatio;
    }

    /*好评率保留一位小数位*/
    public String getSatisfyRatio2()
    {
        double value = new BigDecimal(SatisfyRatio * 100).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
        return new StringBuilder().append(value).append("%").toString();
    }


    public void setSatisfyRatio(double SatisfyRatio)
    {
        this.SatisfyRatio = SatisfyRatio;
    }

    public int getStatus()
    {
        return Status;
    }

    public void setStatus(int Status)
    {
        this.Status = Status;
    }

    public String getLastActivation()
    {
        return LastActivation;
    }

    public void setLastActivation(String LastActivation)
    {
        this.LastActivation = LastActivation;
    }

    public boolean isShowOffice()
    {
        return ShowOffice;
    }

    public void setShowOffice(boolean ShowOffice)
    {
        this.ShowOffice = ShowOffice;
    }

    public int getAnswerCount()
    {
        return AnswerCount;
    }

    public void setAnswerCount(int AnswerCount)
    {
        this.AnswerCount = AnswerCount;
    }

    public String getTagTime()
    {
        return TagTime;
    }

    public void setTagTime(String TagTime)
    {
        this.TagTime = TagTime;
    }

    public String getCareerTime()
    {
        return CareerTime;
    }

    public void setCareerTime(String CareerTime)
    {
        this.CareerTime = CareerTime;
    }

    public String getTag()
    {
        return Tag;
    }

    public void setTag(String Tag)
    {
        this.Tag = Tag;
    }

    public String getInterDesc()
    {
        return InterDesc;
    }

    public void setInterDesc(String InterDesc)
    {
        this.InterDesc = InterDesc;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(ID);
        dest.writeString(Avatar);
        dest.writeString(NickName);
        dest.writeString(Location);
        dest.writeString(Description);
        dest.writeInt(ProfileID);
        dest.writeString(LawOffice);
        dest.writeInt(ConsPrice);
        dest.writeInt(CallPrice);
        dest.writeInt(LawyerType);
        dest.writeDouble(SatisfyRatio);
        dest.writeInt(Status);
        dest.writeString(LastActivation);
        dest.writeByte((byte) (ShowOffice ? 1 : 0));
        dest.writeInt(AnswerCount);
        dest.writeString(TagTime);
        dest.writeString(CareerTime);
        dest.writeString(Tag);
        dest.writeString(InterDesc);
    }


    /*拼接律师工作地址*/
    public String getWorkAddress()
    {
        return ShowOffice ? TextUtils.concat(Location, " | ", LawOffice).toString() : Location;
    }

    /*拼接律师服务价格*/
    public String getWorkPrice()
    {
        return String.valueOf(ConsPrice / 100);
    }

    /*律师类型*/
    public String getWorkType()
    {
        return LawyerType == 1 ? "执业律师" : "法律顾问";
    }

    /*律师擅长的标签*/
    public String[] getLabel()
    {
        if (TextUtils.isEmpty(Tag)) return null;
        return Tag.split("、");
    }


}
