package com.wei.lawyer.model;

/**
 * 作者：赵若位
 * 时间：2018/12/15 10:41
 * 邮箱：1070138445@qq.com
 * 功能：案件委托初始化对象
 */
public class CaseBean
{
    /**
     * ID : 540
     * Title : 案件委托
     * Description : 444444444444444444
     * Screenshot : https://og0f478en.qnssl.com/delegation_detail_view.png
     * ListView : https://og0f478en.qnssl.com/delegation_list_view.png
     * OrderView : https://archives.cdn.lvlvlaw.com/delegation_list_view.png
     * OriginalPrice : 80000
     * Price : 3000
     * Deduction : 0
     * Category : 600
     * VisitCount : 47452
     * SalesVolume : 2406
     * Content : 1.订金支付成功后，平台工作人员会尽快与您取得联系，匹配擅长您案件的律师； 2.订金可抵扣服务费，若委托未达成，订金作为平台前期服务费，不予退还； 3.为保障您的权益，确认委托后，请付款至平台； 4.切勿使用平台担保支付以外的方式付款，以防发生不必要纠纷。
     * PriceNote : 1、平台运用“法律大数据+AI人工智能”技术，为用户匹配最符合诉求的律师。 2、若用户顾虑无法正确挑选，平台管理员对委托案件也有“标准化、流程化”的处理方案，可由专业人员综合地域、领域、执业年限、律师报价等必要因素，出具挑选律师的专业指导意见，务必保证用户找到称心的律师。
     * TimeNote : 3333333333333333
     */

    private int ID;
    private String Title;
    private String Description;
    private String Screenshot;
    private String ListView;
    private String OrderView;
    private int OriginalPrice;
    private int Price;
    private int Deduction;
    private int Category;
    private int VisitCount;
    private int SalesVolume;
    private String Content;
    private String PriceNote;
    private String TimeNote;

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public String getTitle()
    {
        return Title;
    }

    public void setTitle(String Title)
    {
        this.Title = Title;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String Description)
    {
        this.Description = Description;
    }

    public String getScreenshot()
    {
        return Screenshot;
    }

    public void setScreenshot(String Screenshot)
    {
        this.Screenshot = Screenshot;
    }

    public String getListView()
    {
        return ListView;
    }

    public void setListView(String ListView)
    {
        this.ListView = ListView;
    }

    public String getOrderView()
    {
        return OrderView;
    }

    public void setOrderView(String OrderView)
    {
        this.OrderView = OrderView;
    }

    public int getOriginalPrice()
    {
        return OriginalPrice;
    }

    public void setOriginalPrice(int OriginalPrice)
    {
        this.OriginalPrice = OriginalPrice;
    }

    public int getPrice()
    {
        return Price;
    }

    public void setPrice(int Price)
    {
        this.Price = Price;
    }

    public int getDeduction()
    {
        return Deduction;
    }

    public void setDeduction(int Deduction)
    {
        this.Deduction = Deduction;
    }

    public int getCategory()
    {
        return Category;
    }

    public void setCategory(int Category)
    {
        this.Category = Category;
    }

    public int getVisitCount()
    {
        return VisitCount;
    }

    public void setVisitCount(int VisitCount)
    {
        this.VisitCount = VisitCount;
    }

    public int getSalesVolume()
    {
        return SalesVolume;
    }

    public void setSalesVolume(int SalesVolume)
    {
        this.SalesVolume = SalesVolume;
    }

    public String getContent()
    {
        return Content;
    }

    public void setContent(String Content)
    {
        this.Content = Content;
    }

    public String getPriceNote()
    {
        return PriceNote;
    }

    public void setPriceNote(String PriceNote)
    {
        this.PriceNote = PriceNote;
    }

    public String getTimeNote()
    {
        return TimeNote;
    }

    public void setTimeNote(String TimeNote)
    {
        this.TimeNote = TimeNote;
    }
}
