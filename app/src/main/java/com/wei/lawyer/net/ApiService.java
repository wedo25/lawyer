package com.wei.lawyer.net;

import com.wei.lawyer.model.BannerBean;
import com.wei.lawyer.model.BaseDataBean;
import com.wei.lawyer.model.BaseResultBean;
import com.wei.lawyer.model.BookBean;
import com.wei.lawyer.model.CaseBean;
import com.wei.lawyer.model.CompanyServiceBean;
import com.wei.lawyer.model.ConfigBean;
import com.wei.lawyer.model.ContractBean;
import com.wei.lawyer.model.FieldBean;
import com.wei.lawyer.model.LawyerBean;
import com.wei.lawyer.model.LawyerClassBean;
import com.wei.lawyer.model.LawyerCommentBean;
import com.wei.lawyer.model.TokenBean;
import com.wei.lawyer.model.UserBean;

import java.util.List;

import io.reactivex.Flowable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * 作者：赵若位
 * 时间：2018/11/28 17:56
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public interface ApiService
{
    /*Application*/
    @GET("api/v1/appconfig")
    Flowable<BaseDataBean<ConfigBean>> getConfig();


    /*Home*/
    //Banner
    @GET("api/v1/advertisements/postads")
    Flowable<BaseDataBean<List<BannerBean>>> getBannerData();

    //服务动态
    @GET("api/v1/lawyerinfos/online?page=1&pageSize=20")
    Flowable<BaseResultBean<List<LawyerBean>>> getLawyerServiceData();

    //推荐律师
    @GET("api/v1/lawyerinfos?location=&tag=&order=0&page=1&pageSize=20")
    Flowable<BaseResultBean<List<LawyerBean>>> getLawyerRecommentData();

    //法律小讲堂
    @GET("api/v1/lawtips?&pageSize=1")
    Flowable<BaseResultBean<List<LawyerClassBean>>> getLawclassData(@Query("page") int page);

    //案件维托
    @GET("api/v1/delegation")
    Flowable<BaseDataBean<CaseBean>> getCaseData();

    //文书服务
    @GET("api/v1/cusdocument")
    Flowable<BaseDataBean<BookBean>> getBookData();

    //合同模板分类
    @GET("api/v1/contract_categories")
    Flowable<BaseDataBean<List<FieldBean>>> getContractClassification();

    //合同模板列表
    @GET("api/v1/contract_categories/{ContractID}/contracts?pageSize=20")
    Flowable<BaseResultBean<List<ContractBean>>> getContractList(@Path("ContractID") int id,
                                                                 @Query("page") int page);

    //合同详情页数据
    @GET("api/v1/contract/{ContractID}")
    Flowable<BaseDataBean<ContractBean>> getContractDetail(@Path("ContractID") int contractID);

    //合同详情页-查询该合同是否已被收藏
    @GET("api/v1/collection?targetType=3")
    Flowable<BaseResultBean<Boolean>> getContractCollection(@Query("targetId") int targetId);


    //合同详情-收藏
    @POST("api/v1/collections")
    @FormUrlEncoded
    Flowable<BaseDataBean<Object>> collection(@Field("targetId") int contractID,
                                              @Field("targetType") int targetType);

    //合同详情-取消收藏
    @DELETE("api/v1/collection?targetType=3")
    Flowable<BaseDataBean<Object>> unCollection(@Query("targetId") int contractID);

    //合同推荐
    @GET("api/v1/contracts/hottest")
    Flowable<BaseDataBean<List<ContractBean>>> getRecommentContractData();

    //合同搜索
    @POST("api/v1/contracts/search?page=1&pageSize=20")
    @FormUrlEncoded
    Flowable<BaseResultBean<List<ContractBean>>> getContractSearch(@Field("keyword") String descript);

    //企业服务-法律服务
    @GET("api/v1/services?page=1&pageSize=20")
    Flowable<BaseResultBean<List<CompanyServiceBean>>> getCompanyServiceData();

    //企业服务-法律培训
    @GET("api/v1/trainings?page=1&pageSize=20")
    Flowable<BaseResultBean<List<CompanyServiceBean>>> getCompanyTrainingData();


    //企业服务-详情
    @GET("api/v1/service/{CompanyID}")
    Flowable<BaseDataBean<CompanyServiceBean>> getCompanyDetailData(@Path("CompanyID") int id);

    //企业服务-评论
    @GET("api/v1/contract/{CompanyID}/comments?page=1&pageSize=20")
    Flowable<BaseResultBean<List<LawyerCommentBean>>> getCompanyCommentData(@Path("CompanyID") int id);


    /*Find*/
    //获取律师列表数据
    @GET("api/v1/lawyerinfos?pageSize=20")
    Flowable<BaseResultBean<List<LawyerBean>>> getLawyerList(@Query("page") int page, @Query("order") int order,
                                                             @Query("tag") String tag, @Query("location") String location);

    //获取律师详情数据
    @GET("api/v1/lawyerinfo/{ProfileID}")
    Flowable<BaseDataBean<LawyerBean>> getLawyerDetail(@Path("ProfileID") int ProfileID);

    //获取律师详情页评论
    @GET("api/v1/user/{ProfileID}/conscomments?pageSize=20")
    Flowable<BaseResultBean<List<LawyerCommentBean>>> getLawyerDetailComment(@Path("ProfileID") int ProfileID, @Query("page") int page);

    //获取律师擅长技能的类型
    @GET("api/v1/consultationtags")
    Flowable<BaseDataBean<List<FieldBean>>> getLawyerField();








    /*Commit*/


    /*User*/
    //登录
    @POST("api/v1/users/login")
    @FormUrlEncoded
    Flowable<BaseDataBean<TokenBean>> login(@Field("account") String account, @Field("password") String passWord);

    //获取验证码
    @POST("api/v1/users/vcode")
    @FormUrlEncoded
    Flowable<BaseDataBean<Object>> getPhoneCode(@Field("account") String account, @Field("reset") Boolean reset);

    //忘记密码
    @POST("api/v1/users/forgetpassword")
    @FormUrlEncoded
    Flowable<BaseDataBean<Object>> forgetPassWord(@Field("account") String account, @Field("password") String password,
                                                  @Field("vcode") String code);

    //注册
    @POST("api/v1/users/register")
    @FormUrlEncoded
    Flowable<BaseDataBean<TokenBean>> register(@Field("account") String account, @Field("password") String password,
                                               @Field("nickname") String nickname, @Field("vcode") String code);

    //获取用户信息
    @GET("api/v1/users/profile")
    Flowable<BaseDataBean<UserBean>> getUserInfo();

    //获取用户钱包信息
    @GET("api/v1/users/asset")
    Flowable<BaseDataBean<UserBean>> getUserAsset();

    //修改用户头像
    @PUT("api/v1/users/avatar")
    Flowable<BaseResultBean<String>> setAvatar(@Body RequestBody body);

    //修改用户资料
    @PUT("api/v1/users/basicprofile")
    Flowable<BaseDataBean<Object>> modifyUserData(@Body RequestBody body);

}
