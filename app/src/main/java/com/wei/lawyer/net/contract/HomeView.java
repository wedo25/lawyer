package com.wei.lawyer.net.contract;

import com.wei.lawyer.model.BannerBean;
import com.wei.lawyer.model.BaseDataBean;
import com.wei.lawyer.model.LawyerBean;
import com.wei.lawyer.model.LawyerClassBean;
import com.wei.lawyer.net.BaseView;

import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/12/7 15:50
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public interface HomeView extends BaseView
{
    /*Banner*/
    void showBannerData(BaseDataBean<List<BannerBean>> data);

    /*服务动态*/
    void showServiceData(BaseDataBean<List<LawyerBean>> data);

    /*推荐律师*/
    void showRecommentData(List<LawyerBean> list);

    /*法律小课堂*/
    void showLawClassData(LawyerClassBean data);
}
