package com.wei.lawyer.net.contract;

import com.wei.lawyer.model.ContractBean;
import com.wei.lawyer.model.ContractSearchBean;
import com.wei.lawyer.net.BaseView;

import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/12/19 16:39
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public interface ContractSearchView extends BaseView
{

    /*显示推荐合同列表*/
    void showRecommentContract(List<ContractSearchBean> list);

    /*显示搜索历史纪录列表*/
    void showHistoryContract(List<ContractBean> list);

    /*显示搜索列表*/
    void showSearchContract(List<ContractBean> list);
}
