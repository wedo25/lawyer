package com.wei.lawyer.net.presenter;

import com.trello.rxlifecycle2.LifecycleProvider;
import com.wei.lawyer.model.BaseDataBean;
import com.wei.lawyer.model.BaseResultBean;
import com.wei.lawyer.model.FieldBean;
import com.wei.lawyer.model.LawyerBean;
import com.wei.lawyer.model.LawyerCommentBean;
import com.wei.lawyer.model.LawyerSearchBean;
import com.wei.lawyer.model.UserBean;
import com.wei.lawyer.net.BaseObserver;
import com.wei.lawyer.net.BasePresenter;
import com.wei.lawyer.net.BaseView;
import com.wei.lawyer.net.RetrofitManager;
import com.wei.lawyer.utils.RxUtils;

import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/11/28 19:29
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class FindPresenter extends BasePresenter<BaseView>
{
    public FindPresenter(BaseView view, LifecycleProvider provider)
    {
        super(view, provider);
    }


    /**
     * 查询律师列表数据
     */
    public void getLawyerList(LawyerSearchBean search)
    {
        add(RetrofitManager.getManager().getService()
                .getLawyerList(search.getPage(), search.getOrder(), search.getTag(), search.getLocation())
                .compose(this.<BaseResultBean<List<LawyerBean>>>loadTransformer())
                .compose(RxUtils.<List<LawyerBean>>handleBaseResult())
                .subscribeWith(new BaseObserver<List<LawyerBean>>(mView)
                {
                    @Override
                    public void onNext(List<LawyerBean> list)
                    {
                        if (mView != null)
                        {
                            mView.showData(list);
                        }
                    }
                }));
    }


    /**
     * 获取律师详情
     *
     * @param id 律师ProfileID
     */
    public void getLawyerDetail(int id)
    {
        add(
                RetrofitManager.getManager().getService().getLawyerDetail(id)
                        .compose(this.<BaseDataBean<LawyerBean>>loadTransformer())
                        .compose(RxUtils.<LawyerBean>handleBaseData())
                        .subscribeWith(new BaseObserver<LawyerBean>(mView)
                        {
                            @Override
                            public void onNext(LawyerBean data)
                            {
                                if (mView != null)
                                {
                                    mView.showData(data);
                                }
                            }
                        })
        );
    }


    /**
     * 获取律师详情页面评论
     *
     * @param id 律师ProfileID
     */
    public void getLawyerDetailComment(int id, int page)
    {
        add(
                RetrofitManager.getManager().getService().getLawyerDetailComment(id, page)
                        .compose(this.<BaseResultBean<List<LawyerCommentBean>>>loadTransformer())
                        .compose(RxUtils.<List<LawyerCommentBean>>handleBaseResult())
                        .subscribeWith(new BaseObserver<List<LawyerCommentBean>>(mView)
                        {
                            @Override
                            public void onNext(List<LawyerCommentBean> list)
                            {
                                if (mView != null)
                                {
                                    mView.showData(list);
                                }
                            }
                        })
        );
    }


    /**
     * 获取律师擅长的技能范围
     */
    public void getLawyerSkill()
    {
        add(
                RetrofitManager.getManager().getService().getLawyerField()
                        .compose(this.<BaseDataBean<List<FieldBean>>>loadTransformer())
                        .compose(RxUtils.<List<FieldBean>>handleBaseData())
                        .subscribeWith(new BaseObserver<List<FieldBean>>(mView)
                        {
                            @Override
                            public void onNext(List<FieldBean> list)
                            {
                                if (mView != null)
                                {
                                    mView.showData(list);
                                }
                            }
                        })
        );
    }


    /**
     * 获取用户余额信息
     */
    public void getUserAssets()
    {
        add(
                RetrofitManager.getManager().getService().getUserAsset()
                        .compose(this.<BaseDataBean<UserBean>>loadTransformer())
                        .compose(RxUtils.<UserBean>handleBaseData())
                        .subscribeWith(new BaseObserver<UserBean>(mView)
                        {
                            @Override
                            public void onNext(UserBean user)
                            {
                                if (mView != null)
                                {
                                    mView.showData(user);
                                }
                            }
                        })
        );
    }
}
