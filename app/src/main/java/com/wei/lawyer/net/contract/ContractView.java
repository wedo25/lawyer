package com.wei.lawyer.net.contract;

import com.wei.lawyer.model.ContractBean;
import com.wei.lawyer.model.FieldBean;
import com.wei.lawyer.net.BaseView;

import java.util.List;

/**
 * 作者：赵若位
 * 时间：2018/12/17 11:47
 * 邮箱：1070138445@qq.com
 * 功能：合同模板
 */
public interface ContractView extends BaseView
{
    void showClassification(List<FieldBean> list);


    void showContractData(List<ContractBean> list);
}
