package com.wei.lawyer.net.contract;

import com.wei.lawyer.model.ContractBean;
import com.wei.lawyer.net.BaseView;

/**
 * 作者：赵若位
 * 时间：2018/12/18 17:17
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public interface ContractDetailView extends BaseView
{
    /*合同详情页数据*/
    void showContractDetail(ContractBean data);

    /*收藏该合同*/
    void showCollection();

    /*取消收藏该合同*/
    void showUnCollection();
}
