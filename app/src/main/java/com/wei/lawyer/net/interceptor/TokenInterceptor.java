package com.wei.lawyer.net.interceptor;

import android.text.TextUtils;

import com.wei.lawyer.App;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 作者：赵若位
 * 时间：2018/11/30 9:57
 * 邮箱：1070138445@qq.com
 * 功能：添加固定的请求头
 */
public class TokenInterceptor implements Interceptor
{
    private static final String AUTHORIZATION = "Authorization";

    public TokenInterceptor()
    {

    }

    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request request = chain.request();
        if (TextUtils.isEmpty(App.mToken))
        {
            return chain.proceed(request);
        } else
        {
            return chain.proceed(request.newBuilder().addHeader(AUTHORIZATION, App.mToken)
                    .method(request.method(), request.body()).build());
        }
    }
}
